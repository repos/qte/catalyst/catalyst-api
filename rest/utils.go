package rest

import (
	"fmt"
	"log"
	"net/http"
	"runtime/debug"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/go-errors/errors"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/catalystError"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/service"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"
)

func parseIdParam(c *gin.Context) (uint, error) {
	id, err := utils.ParseUint(c.Param("id"))
	if err != nil {
		return 0, catalystError.InvalidId{Err: errors.New(err)}
	}
	return id, nil
}

func parseStreamQueryParam(streamParam string) (service.StreamParams, error) {
	appContainer := strings.Split(streamParam, "/")
	if len(appContainer) != 2 || len(appContainer[0]) < 1 || len(appContainer[1]) < 1 {
		return service.StreamParams{}, catalystError.InvalidQueryParam{
			Err: errors.Errorf("could not parse app name and container name from stream parameter '%s'", streamParam),
		}
	}

	return service.StreamParams{AppName: appContainer[0], ContainerName: appContainer[1]}, nil
}

func handleError(c *gin.Context, err error) {
	if err == nil {
		return
	}

	switch errv := err.(type) {
	case catalystError.DeletedEntry:
		_ = c.AbortWithError(http.StatusGone, errv)
	case catalystError.UnknownEntry:
		_ = c.AbortWithError(http.StatusNotFound, errv)
	case catalystError.InvalidRequestData:
		_ = c.AbortWithError(http.StatusUnprocessableEntity, errv)
	case catalystError.Unauthenticated:
		_ = c.AbortWithError(http.StatusUnauthorized, errv)
	case catalystError.Unauthorized:
		_ = c.AbortWithError(http.StatusForbidden, errv)
	case catalystError.ClientError:
		_ = c.AbortWithError(http.StatusBadRequest, errv)
	case catalystError.ContainerNotStarted:
		_ = c.AbortWithError(http.StatusServiceUnavailable, errv)
	case catalystError.CatalystError:
		//TODO: It would be interesting to look into any library options to use an MDC (Mapped
		// Diagnostic Context) here. An MDC could be used to enrich the log with useful info:
		// affected environment, node, etc
		log.Println(errv.ErrorStack())
		c.AbortWithStatus(http.StatusInternalServerError)
	default:
		wrongErrMsg := fmt.Sprintf(
			"Error is of type '%T' instead of `catalystError.CatalystError`. This should not have happened:\n%v",
			errv, errv,
		)
		log.Println(wrongErrMsg, "\n", string(debug.Stack()))
		c.AbortWithStatus(http.StatusInternalServerError)
	}
}
