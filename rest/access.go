package rest

import (
	"github.com/gin-gonic/gin"
	"github.com/go-errors/errors"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/catalystError"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/service"
)

type Access struct {
	ct *service.Catalyst
}

func NewAccess(ct *service.Catalyst) *Access {
	return &Access{
		ct: ct,
	}
}

func (a *Access) AuthorizeEnvAccess(c *gin.Context) {
	currTokenId, err := GetAuthenticatedTokenId(c)
	if err != nil {
		handleError(c, err)
		return
	}

	if *currTokenId == AdminId {
		return
	}

	id, err := parseIdParam(c)
	if err != nil {
		handleError(c, err)
		return
	}
	env, envErr := a.ct.GetEnvironmentDeleted(id, true)
	if env != nil && env.OwnerId != nil && *currTokenId == *env.OwnerId {
		if envErr != nil {
			handleError(c, err)
		}
	} else {
		err = catalystError.Unauthorized{
			Err: errors.Errorf("No access to environment %d", id),
		}
		handleError(c, err)
	}
}

func GetAuthenticatedTokenId(c *gin.Context) (*uint, error) {
	if currTokenId, ok := c.Get("token.id"); !ok {
		return nil, errors.Errorf("No authentication happened but was required")
	} else {
		currTokenIdUint := currTokenId.(uint)
		return &currTokenIdUint, nil
	}
}
