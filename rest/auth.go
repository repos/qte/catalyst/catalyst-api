package rest

import (
	"fmt"
	"os"
	"regexp"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/go-errors/errors"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/catalystError"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/crypto"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/service"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"
)

const AdminId uint = 0
const tokenPrefix = "ApiToken"

var baseTokenFormat = `-\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$`

// id-uuid (e.g. 3-550e8400-e29b-41d4-a716-446655440000)
var userTokenFormat = regexp.MustCompile(`^\d+` + baseTokenFormat)
var adminTokenFormat = regexp.MustCompile(fmt.Sprintf(`^%d%s`, AdminId, baseTokenFormat))
var adminToken string

type Auth struct {
	hash crypto.Hash
	ct   *service.Catalyst
}

func NewAuth(hash crypto.Hash, ct *service.Catalyst) *Auth {
	adminToken = os.Getenv("ADMIN_TOKEN")
	return &Auth{
		hash: hash,
		ct:   ct,
	}
}

func VerifyAdminToken() {
	if !adminTokenFormat.MatchString(adminToken) {
		panic(fmt.Sprintf(`"%s" is not a valid admin token`, adminToken))
	}
}

// TODO: Could make sense to move this to `access.go`
func (a *Auth) AuthenticateAdmin(c *gin.Context) {
	handleReqToken := func(_ uint) {
		err := catalystError.Unauthorized{
			Err: errors.Errorf("Admin access required"),
		}
		handleError(c, err)
	}

	a.authenticate(c, handleReqToken)
}

func (a *Auth) AuthenticateUser(c *gin.Context) {
	handleReqToken := func(reqTokenId uint) {
		c.Set("token.id", reqTokenId)
	}

	a.authenticate(c, handleReqToken)
}

func (a *Auth) authenticate(c *gin.Context, tokenHandler func(uint)) {
	token, ok := extractToken(c)
	if !ok {
		handleNonValidToken(c)
		return
	}

	if token == adminToken {
		c.Set("token.id", AdminId)
		return
	}

	tokenParts := strings.SplitN(token, "-", 2)
	tokenId, _ := utils.ParseUint(tokenParts[0])
	tokenEntry, err := a.ct.GetToken(tokenId)
	if err != nil {
		if _, ok = err.(catalystError.UnknownEntry); ok {
			handleNonValidToken(c)
		} else {
			handleError(c, err)
		}
		return
	}

	if a.hash.Match(token, tokenEntry.EncodedHash) {
		tokenHandler(tokenId)
	} else {
		handleNonValidToken(c)
	}
}

func extractToken(c *gin.Context) (string, bool) {
	authHeader := c.Request.Header.Get("Authorization")
	headerParts := strings.Split(authHeader, " ")
	if len(headerParts) != 2 || headerParts[0] != tokenPrefix {
		return "", false
	}

	reqToken := headerParts[1]
	if !userTokenFormat.MatchString(reqToken) {
		return "", false
	}
	return reqToken, true
}

func handleNonValidToken(c *gin.Context) {
	err := catalystError.Unauthenticated{
		Err: errors.Errorf("Invalid or no ApiToken provided"),
	}
	handleError(c, err)
}
