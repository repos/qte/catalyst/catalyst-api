package rest

import (
	"io"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-errors/errors"
	"github.com/go-playground/validator/v10"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/crypto"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/kubernetes"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/model"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/service"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/validation"
)

var customValidator *validation.CustomValidations

type errorResponse struct {
	StatusCode int      `json:"statusCode"`
	StatusText string   `json:"statusText"`
	Details    []string `json:"details"`
}
type Router struct {
	kchrts kubernetes.KnownCharts
	hash   crypto.Hash
	auth   *Auth
	access *Access
	envSvc *service.EnvironmentService
	ct     *service.Catalyst
}

func NewRouter(
	kchrts kubernetes.KnownCharts,
	hash crypto.Hash,
	auth *Auth,
	access *Access,
	envSvc *service.EnvironmentService,
	ct *service.Catalyst,
) *Router {
	return &Router{
		kchrts: kchrts,
		hash:   hash,
		auth:   auth,
		access: access,
		envSvc: envSvc,
		ct:     ct,
	}
}

func (r *Router) RunRouter(address string) {
	router := r.setUpRouter()
	err := router.Run(address)
	utils.HandleError(err)
}

func (r *Router) setUpRouter() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	router.Use(addErrorsToResponse)
	router.Any("/healthz", func(c *gin.Context) { c.Status(200) })

	// Endpoints below in same order as openapi.yaml
	api := router.Group("/api")
	api.GET("/charts", r.getCharts)
	api.GET("/charts/:name", r.getChart)

	environments := api.Group("/environments")
	// Note that middleware needs to be added before the regular handlers or it won't work
	environments.Use(r.auth.AuthenticateUser)
	environments.GET("", r.getEnvs)
	environments.POST("", r.createEnv)
	environments.GET("/:id", r.access.AuthorizeEnvAccess, r.getEnv)
	environments.DELETE("/:id", r.access.AuthorizeEnvAccess, r.delEnv)
	environments.GET("/:id/logs", r.access.AuthorizeEnvAccess, r.getEnvLogs)

	apiTokens := api.Group("/apiTokens")
	// Note that middleware needs to be added before the regular handlers or it won't work
	apiTokens.Use(r.auth.AuthenticateAdmin)
	apiTokens.GET("", r.getTokens)
	apiTokens.POST("", r.createToken)
	apiTokens.GET("/:id", r.getToken)

	customValidator = validation.SetupCustomValidations(validation.Context{
		KnownCharts: r.kchrts,
		EnvService:  r.envSvc,
	})

	return router
}

func (r *Router) createEnv(c *gin.Context) {
	var env model.Environment
	if err := c.BindJSON(&env); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	if _, err := customValidator.ValidateEnvParameters(&env); err != nil {
		handleError(c, err)
		return
	}

	currTokenId, err := GetAuthenticatedTokenId(c)
	if err != nil {
		handleError(c, err)
		return
	}
	if *currTokenId != AdminId {
		env.OwnerId = currTokenId
	}

	if err := r.ct.CreateEnvironmentAsync(&env); err != nil {
		handleError(c, err)
	} else {
		c.JSON(http.StatusAccepted, env)
	}
}

func (r *Router) getEnv(c *gin.Context) {
	id, err := parseIdParam(c)
	if err != nil {
		handleError(c, err)
		return
	}
	env, err := r.ct.GetEnvironment(id)
	if err != nil {
		handleError(c, err)
		return
	}
	c.JSON(http.StatusOK, env)
}

func (r *Router) delEnv(c *gin.Context) {
	id, err := parseIdParam(c)
	if err != nil {
		handleError(c, err)
		return
	}
	err = r.ct.DeleteEnvironment(id)
	if err != nil {
		handleError(c, err)
		return
	}
	c.JSON(http.StatusOK, "Environment successfully deleted")
}

func (r *Router) getEnvs(c *gin.Context) {
	currTokenId, err := GetAuthenticatedTokenId(c)
	if err != nil {
		handleError(c, err)
		return
	}

	var envs *[]model.Environment
	if *currTokenId == AdminId {
		envs, err = r.ct.GetAllEnvironments()
	} else {
		envs, err = r.ct.GetEnvironmentsByOwner(*currTokenId)
	}
	if err != nil {
		handleError(c, err)
		return
	}
	c.JSON(http.StatusOK, envs)
}

func (r *Router) streamLogs(c *gin.Context, id uint, streamParams []string) {
	var containerNames []service.StreamParams
	for _, streamParam := range streamParams {
		params, err := parseStreamQueryParam(streamParam)
		if err != nil {
			handleError(c, err)
			return
		}
		containerNames = append(containerNames, params)
	}
	c.Stream(func(w io.Writer) bool {
		r.ct.StreamContainerLogs(
			id,
			containerNames,
			func(err error) {
				handleError(c, err)
			},
			func(logs kubernetes.PodContainerLogs) {
				c.SSEvent("containerLogs", logs)
				c.Writer.Flush()
			},
		)
		return false
	})
}

func (r *Router) getEnvLogs(c *gin.Context) {
	paramPairs := c.Request.URL.Query()
	streamParams := paramPairs["stream"]
	id, err := parseIdParam(c)
	if err != nil {
		handleError(c, err)
		return
	}

	if len(streamParams) > 0 {
		r.streamLogs(c, id, streamParams)
	} else {
		logs, err := r.ct.GetEnvironmentLogs(id)
		if err != nil {
			handleError(c, err)
			return
		}
		c.JSON(http.StatusOK, logs)
	}
}

func (r *Router) getCharts(c *gin.Context) {
	c.JSON(http.StatusOK, r.ct.GetCharts())
}

func (r *Router) getChart(c *gin.Context) {
	chartName := c.Param("name")
	if !customValidator.ChartIsKnown(chartName) {
		err := errors.Errorf("unknown chart: %s", chartName)
		_ = c.AbortWithError(http.StatusNotFound, err)
		return
	}

	chartDetails, err := r.ct.GetChart(chartName)
	if err != nil {
		handleError(c, err)
		return
	}
	c.JSON(http.StatusOK, chartDetails)
}

func (r *Router) getTokens(c *gin.Context) {
	tokens, err := r.ct.GetTokens()
	if err != nil {
		handleError(c, err)
		return
	}
	c.JSON(http.StatusOK, tokens)
}

func (r *Router) createToken(c *gin.Context) {
	var token model.Token
	if err := c.BindJSON(&token); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	if value, err := r.ct.CreateToken(&token); err != nil {
		handleError(c, err)
	} else {
		tokenResponse := model.TokenCreationResponse{
			Token: token,
			Value: value,
		}
		c.JSON(http.StatusCreated, tokenResponse)
	}
}

func (r *Router) getToken(c *gin.Context) {
	id, err := parseIdParam(c)
	if err != nil {
		handleError(c, err)
		return
	}

	token, err := r.ct.GetToken(id)
	if err != nil {
		handleError(c, err)
		return
	}
	c.JSON(http.StatusOK, token)
}

func addErrorsToResponse(c *gin.Context) {
	c.Next()

	if len(c.Errors) > 0 {
		var errs []string
		for _, err := range c.Errors {
			unwrappedErr := err.Err
			var valErrors validator.ValidationErrors
			if errors.As(unwrappedErr, &valErrors) {
				for _, valErr := range valErrors {
					if errFunc, ok := validation.CustomErrors[valErr.Tag()]; ok {
						errs = append(errs, errFunc(valErr).Error())
					} else {
						errs = append(errs, valErr.Error())
					}
				}
			} else {
				errs = append(errs, unwrappedErr.Error())
			}
		}

		errorResponse := errorResponse{
			StatusCode: c.Writer.Status(),
			StatusText: http.StatusText(c.Writer.Status()),
			Details:    errs,
		}

		c.JSON(c.Writer.Status(), errorResponse)
	}
}
