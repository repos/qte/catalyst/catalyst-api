.PHONY: test
SHELL := /bin/bash
VERSION = $(shell cat VERSION)
PACKAGE := gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api
TEST_DB_USER ?= root
TEST_DB_PASS ?= supersecret
TEST_DB_HOST ?= localhost
TEST_DB_PORT ?= 3306
TEST_DB_NAME ?= catalyst

GO_LDFLAGS = \
  "-X '$(PACKAGE)/meta.Version=$(VERSION)'"

db:
	ROOT_PASS=$(TEST_DB_PASS) DB_NAME=$(TEST_DB_NAME) DB_PORT=$(TEST_DB_PORT) docker compose up -d db

db-clean:
	docker compose down -v db

test:
	go list -f '{{range .GoFiles}}{{printf "%s/%s\n" $$.Dir .}}{{end}}{{range .XTestGoFiles}}{{printf "%s/%s\n" $$.Dir .}}{{end}}' ./... | while read f; do gofmt -e -d "$$f"; done

bare-bdd:
	TEST_DB_USER=$(TEST_DB_USER) TEST_DB_PASS=$(TEST_DB_PASS) TEST_DB_HOST=$(TEST_DB_HOST) TEST_DB_PORT=$(TEST_DB_PORT) TEST_DB_DATABASE=$(TEST_DB_NAME) \
	go run github.com/onsi/ginkgo/v2/ginkgo -r --randomize-all --randomize-suites --fail-on-pending --fail-on-empty --keep-going

bdd-local: db bare-bdd

bdd-ci: bare-bdd

bdd: bdd-local

download:
	go mod download

build:
	go build -v -ldflags=$(GO_LDFLAGS)

mocks:
	go run go.uber.org/mock/mockgen -source=kubernetes/kubeWrapper.go -destination=bdd/mocks/kube.go -package=mocks
	go run go.uber.org/mock/mockgen -source=kubernetes/helmWrapper.go -destination=bdd/mocks/helm.go -package=mocks

clean: db-clean