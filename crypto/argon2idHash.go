package crypto

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"strings"

	"golang.org/x/crypto/argon2"
)

type Argon2idHash struct {
	config   Argon2idConfig
	saltFunc Salt
}

type Argon2idConfig struct {
	Iterations  uint32
	MemoryInKb  uint32
	Parallelism uint8
	KeySize     uint32
	SaltSize    uint32
}

func DefaultArgon2idHash(saltF Salt) Hash {
	return NewArgon2idHash(
		Argon2idConfig{
			Iterations:  20,
			MemoryInKb:  8 * 1024,
			Parallelism: 4,
			KeySize:     32,
			SaltSize:    16,
		},
		saltF,
	)
}

func NewArgon2idHash(config Argon2idConfig, saltF Salt) Hash {
	return &Argon2idHash{config, saltF}
}

func (a2 *Argon2idHash) EncodedHash(secret string) (string, error) {
	salt, hash, err := a2.Hash(secret)
	if err != nil {
		return "", err
	}
	return a2.Encode(salt, hash), nil
}

func (a2 *Argon2idHash) Hash(secret string) ([]byte, []byte, error) {
	cfg := a2.config
	salt, err := a2.saltFunc.GetSalt(cfg.SaltSize)
	if err != nil {
		return nil, nil, err
	}

	hash := a2.hash(salt, secret)
	return salt, hash, nil
}

func (a2 *Argon2idHash) Encode(salt []byte, hash []byte) string {
	cfg := a2.config
	b64Salt := base64.RawStdEncoding.EncodeToString(salt)
	b64Hash := base64.RawStdEncoding.EncodeToString(hash)

	return fmt.Sprintf(
		"$argon2id$v=%d$m=%d,t=%d,p=%d$%s$%s",
		argon2.Version,
		cfg.MemoryInKb,
		cfg.Iterations,
		cfg.Parallelism,
		b64Salt,
		b64Hash,
	)
}

func (a2 *Argon2idHash) Decode(encodedHash string) ([]byte, []byte) {
	cmp := strings.Split(encodedHash, "$")
	b64Salt := cmp[len(cmp)-2]
	b64Hash := cmp[len(cmp)-1]
	salt, _ := base64.RawStdEncoding.DecodeString(b64Salt)
	hash, _ := base64.RawStdEncoding.DecodeString(b64Hash)

	return salt, hash
}

func (a2 *Argon2idHash) Match(secret string, encodedHash string) bool {
	salt, hash := a2.Decode(encodedHash)
	return bytes.Equal(a2.hash(salt, secret), hash)
}

func (a2 *Argon2idHash) hash(salt []byte, secret string) []byte {
	cfg := a2.config
	return argon2.IDKey(
		[]byte(secret),
		salt,
		cfg.Iterations,
		cfg.MemoryInKb,
		cfg.Parallelism,
		cfg.KeySize,
	)
}
