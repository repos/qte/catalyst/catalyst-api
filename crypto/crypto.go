package crypto

type Hash interface {
	EncodedHash(secret string) (string, error)
	Hash(secret string) ([]byte, []byte, error)
	Encode(salt []byte, hash []byte) string
	Decode(encodedHash string) ([]byte, []byte)
	Match(secret string, encodedHash string) bool
}

type Salt interface {
	GetSalt(size uint32) ([]byte, error)
}
