package crypto

import (
	"crypto/rand"

	"github.com/go-errors/errors"
)

// Uses Go's crypto/rand to generate salts, which on Linux platforms relies on /dev/urandom:
// https://pkg.go.dev/crypto/rand#pkg-variables

type RandSalt struct{}

func (g RandSalt) GetSalt(size uint32) ([]byte, error) {
	salt := make([]byte, size)
	_, err := rand.Read(salt)
	if err != nil {
		return nil, errors.New(err)
	}
	return salt, nil
}
