package catalystError

type CatalystError interface {
	error
	ErrorStack() string
}

type ClientError interface {
	CatalystError
	clientError()
}

type ServerError interface {
	CatalystError
	serverError()
}
