package kubernetes

import "gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"

var namespace = utils.GetEnv("K8S_ENV_NAMESPACE", "cat-env")
