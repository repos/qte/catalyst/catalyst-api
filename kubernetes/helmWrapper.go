package kubernetes

import (
	"os"

	"github.com/go-errors/errors"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"
	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/cli"
	"helm.sh/helm/v3/pkg/release"
)

// Wrapper around the Helm client to allow for mocking during testing

type HelmClientWrapper interface {
	InjectClient(actionConfig *action.Configuration)
	GetRelease(relName string, getResourceInfo bool) (*release.Release, error)
	InstallRelease(relName string, chart *chart.Chart, vals map[string]interface{}) (*release.Release, error)
	UpgradeRelease(relName string, chart *chart.Chart, vals map[string]interface{}) (*release.Release, error)
	UninstallRelease(relName string) (*release.UninstallReleaseResponse, error)
}

type HelmWrapperImp struct {
	actionConfig *action.Configuration
}

func NewHelmWrapper(kubeConfig string) HelmClientWrapper {
	settings := cli.New()
	settings.SetNamespace(namespace)

	if os.Getenv("K8S_USE_SERVICE_ACCOUNT") != "true" {
		settings.KubeConfig = kubeConfig
	}

	actionConfig := &action.Configuration{}
	err := actionConfig.Init(
		settings.RESTClientGetter(),
		settings.Namespace(),
		os.Getenv("HELM_DRIVER"),
		// The helm client can become quite verbose, disable logging for now
		func(format string, v ...interface{}) {},
	)
	utils.HandleError(err)

	helmClientWrapper := &HelmWrapperImp{}
	helmClientWrapper.InjectClient(actionConfig)

	return helmClientWrapper
}

func (wi *HelmWrapperImp) InjectClient(actionConfig *action.Configuration) {
	if wi.actionConfig == nil {
		wi.actionConfig = actionConfig
	}
}

func (wi *HelmWrapperImp) GetRelease(relName string, getResourceInfo bool) (*release.Release, error) {
	statusClient := action.NewStatus(wi.actionConfig)
	statusClient.ShowResources = getResourceInfo
	rel, err := statusClient.Run(relName)
	if err != nil {
		return nil, errors.New(err)
	}
	return rel, nil
}

func (wi *HelmWrapperImp) InstallRelease(
	relName string,
	chart *chart.Chart,
	vals map[string]interface{},
) (*release.Release, error) {
	client := action.NewInstall(wi.actionConfig)
	client.ReleaseName = relName
	client.Namespace = namespace
	rel, err := client.Run(chart, vals)
	if err != nil {
		return nil, errors.New(err)
	}
	return rel, nil
}

func (wi *HelmWrapperImp) UpgradeRelease(
	relName string,
	chart *chart.Chart,
	vals map[string]interface{},
) (*release.Release, error) {
	//TODO: If the release is in an inconsistent state ("pending-install", etc), we need to recover
	client := action.NewUpgrade(wi.actionConfig)
	client.Namespace = namespace
	rel, err := client.Run(relName, chart, vals)
	if err != nil {
		return nil, errors.New(err)
	}
	return rel, nil
}

func (wi *HelmWrapperImp) UninstallRelease(relName string) (*release.UninstallReleaseResponse, error) {
	uninstallClient := action.NewUninstall(wi.actionConfig)
	uninstallClient.KeepHistory = true
	return uninstallClient.Run(relName)
}
