package kubernetes

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/codeclysm/extract/v3"
	"github.com/go-errors/errors"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"
	"gopkg.in/yaml.v3"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/chart/loader"
)

var gitlabUrl = utils.GetEnv("GITLAB_URL", "https://gitlab.wikimedia.org")
var projectPathRegex = regexp.MustCompile(gitlabUrl + `/(.+)`)

var chartsLocation = utils.GetEnv("CHARTS_LOCATION", "/tmp/catalyst/charts")
var chartsFileMode = os.FileMode(0755)

type KnownCharts struct {
	Charts []ChartConfig
	// ChartConfig name -> ChartConfig
	ByName map[string]*ChartConfig
}

type ChartConfig struct {
	Name             string         `json:"name"`
	Description      string         `json:"description"`
	GitRepo          string         `yaml:"gitRepo" json:"gitRepo"`
	Directory        string         `json:"directory"`
	CatalystDefaults map[string]any `yaml:"catalystDefaults" json:"catalystDefaults,omitempty"`
}

type ChartDetails struct {
	ChartConfig
	DefaultValues map[string]any `json:"defaultValues"`
}

func (cc *KnownCharts) LoadConfigs() error {
	chCfgPath := utils.GetEnv("CHARTS_CONFIG_LOCATION", "charts.yaml")
	chCfg, err := os.ReadFile(chCfgPath)

	if err != nil {
		return errors.New(err)
	}

	err = yaml.Unmarshal(chCfg, cc)
	if err != nil {
		return errors.New(err)
	}

	cc.ByName = map[string]*ChartConfig{}
	for _, ch := range cc.Charts {
		cc.ByName[ch.Name] = &ch
	}

	err = os.MkdirAll(chartsLocation, chartsFileMode)
	if err != nil {
		return errors.New(err)
	}

	return nil
}

func (chartConfig *ChartConfig) GetDetails() (*ChartDetails, error) {
	chrt, err := chartConfig.LoadInstance(false)
	if err != nil {
		return nil, err
	}

	chartDetails := &ChartDetails{
		ChartConfig:   *chartConfig,
		DefaultValues: chrt.Values,
	}
	return chartDetails, nil
}

func (chartConfig *ChartConfig) LoadInstance(mergeCatalystDefaults bool) (*chart.Chart, error) {
	chrt, err := chartConfig.download()
	if err != nil {
		return nil, err
	}

	if mergeCatalystDefaults && len(chartConfig.CatalystDefaults) > 0 {
		utils.DeepMerge(chrt.Values, chartConfig.CatalystDefaults)
	}

	return chrt, nil
}

func (chartConfig *ChartConfig) download() (*chart.Chart, error) {
	// Local charts are useful during development
	useLocalCharts := utils.GetEnv("USE_LOCAL_CHARTS", "false")
	if skipDownload, _ := strconv.ParseBool(useLocalCharts); skipDownload {
		chartPath := filepath.Join(chartsLocation, chartConfig.Directory)
		chrt, err := loader.Load(chartPath)
		if err != nil {
			return nil, errors.New(err)
		}
		return chrt, nil
	}

	chartDir, err := os.MkdirTemp(chartsLocation, "remoteChart")
	defer os.RemoveAll(chartDir)
	if err != nil {
		return nil, errors.New(err)
	}

	matches := projectPathRegex.FindStringSubmatch(chartConfig.GitRepo)
	if len(matches) < 2 {
		return nil, errors.Errorf(
			"could not parse the repo URL for chart '%s'. Check the chart configuration",
			chartConfig.Name,
		)
	}

	projectPath := url.QueryEscape(matches[1])
	trimmedRemoteDir := strings.TrimSpace(chartConfig.Directory)
	chartQuery :=
		fmt.Sprintf(
			"%s/api/v4/projects/%s/repository/archive.tar.gz?path=%s",
			gitlabUrl,
			projectPath,
			trimmedRemoteDir,
		)
	resp, err := http.Get(chartQuery)
	if err != nil {
		return nil, errors.New(err)
	}

	err = chartConfig.extract(&resp.Body, chartDir)
	if err != nil {
		return nil, err
	}

	chartPath := filepath.Join(chartDir, trimmedRemoteDir)
	chrt, err := loader.Load(chartPath)
	if err != nil {
		return nil, errors.New(err)
	}
	return chrt, nil
}

func (chartConfig *ChartConfig) extract(chartArchive *io.ReadCloser, dst string) error {
	// Removes one level of nesting from the tarball
	var unwrapChart = func(path string) string {
		pathParts := strings.Split(path, "/")
		pathParts = pathParts[1:]
		return strings.Join(pathParts, "/")
	}

	extractor := extract.Extractor{
		FS: fs{},
	}
	err := extractor.Gz(context.TODO(), *chartArchive, dst, unwrapChart)
	if err != nil {
		return errors.Errorf(
			"failed to extract chart '%s' due to: '%w'. Is the chart config correct?",
			chartConfig.Name,
			err,
		)
	}

	return nil
}

type fs struct{}

func (f fs) Link(oldname, newname string) error {
	return os.Link(oldname, newname)
}

func (f fs) MkdirAll(path string, _ os.FileMode) error {
	return os.MkdirAll(path, chartsFileMode)
}

func (f fs) Symlink(oldname, newname string) error {
	return os.Symlink(oldname, newname)
}

func (f fs) OpenFile(name string, flag int, perm os.FileMode) (*os.File, error) {
	return os.OpenFile(name, flag, perm)
}
