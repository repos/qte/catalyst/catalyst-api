package kubernetes

import (
	"bufio"
	"io"
	"slices"
	"strings"

	"github.com/go-errors/errors"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/catalystError"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type PodLogs struct {
	Pod        string             `json:"pod"`
	Containers []PodContainerLogs `json:"containers"`
}

type PodContainerLogs struct {
	Name string     `json:"name"`
	Logs []LogEntry `json:"logs"`
}

type LogEntry struct {
	Timestamp string `json:"timestamp"`
	Log       string `json:"log"`
}

type KubernetesClient struct {
	kubeWrapper KubeClientWrapper
}

func NewKubernetesClient(kubeWrapper KubeClientWrapper) *KubernetesClient {
	client := &KubernetesClient{
		kubeWrapper: kubeWrapper,
	}
	return client
}

func (kc *KubernetesClient) PodLogStream(pod *v1.Pod, containerName string, follow bool) (io.ReadCloser, error) {
	var podLogOptions v1.PodLogOptions

	podLogOptions = v1.PodLogOptions{
		Container:  containerName,
		Follow:     follow,
		Timestamps: true,
	}
	return kc.kubeWrapper.StreamPodLogs(namespace, pod.Name, &podLogOptions)
}

func (kc *KubernetesClient) StreamContainerLogs(pod *v1.Pod, containerName string, logChan chan PodContainerLogs) error {
	foundContainer := false
	for _, contStatus := range append(pod.Status.InitContainerStatuses, pod.Status.ContainerStatuses...) {
		if contStatus.Name == containerName {
			foundContainer = true
			if contStatus.State.Terminated != nil {
				// container ran to completion, etc.
				break
			} else if !*contStatus.Started {
				return catalystError.ContainerNotStarted{Err: errors.Errorf("container '%s' not started", containerName)}
			} else {
				break
			}
		}
	}
	if !foundContainer {
		return catalystError.UnknownContainer{Err: errors.Errorf("container '%s' not found in pod '%s'",
			containerName, pod.Name)}
	}
	stream, err := kc.PodLogStream(pod, containerName, true)
	if err != nil {
		return err
	}
	defer stream.Close()

	err = formatContainerLogs(stream, containerName, logChan)
	if err != nil {
		return err
	}

	return nil
}

func (kc *KubernetesClient) GetPods(labelSelector string) (*v1.PodList, error) {
	pods, err := kc.kubeWrapper.GetPods(namespace, metav1.ListOptions{
		LabelSelector: labelSelector,
	})
	if err != nil {
		return nil, err
	}
	return pods, err
}

func (kc *KubernetesClient) GetLogs(pod *v1.Pod) ([]PodLogs, error) {
	var logs []PodLogs
	var err error
	var readyContainers []string
	for _, contStatuses := range append(pod.Status.InitContainerStatuses, pod.Status.ContainerStatuses...) {
		if contStatuses.Ready {
			readyContainers = append(readyContainers, contStatuses.Name)
		}
	}

	for _, container := range append(pod.Spec.InitContainers, pod.Spec.Containers...) {
		logs, err = func() ([]PodLogs, error) {
			if !slices.Contains(readyContainers, container.Name) {
				return logs, nil
			}
			podLogs, err := kc.PodLogStream(pod, container.Name, false)
			if err != nil {
				return nil, err
			}
			defer podLogs.Close()
			logs, err = formatPodlogs(podLogs, container.Name, logs, pod)
			if err != nil {
				return nil, err
			}
			return logs, nil
		}()
		if err != nil {
			return nil, err
		}

	}

	return logs, nil
}

func formatContainerLogs(logStream io.ReadCloser, containerName string, logChan chan PodContainerLogs) error {
	scanner := bufio.NewScanner(logStream)
	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.SplitN(line, " ", 2)
		if len(parts) == 2 {
			logChan <- PodContainerLogs{Name: containerName, Logs: []LogEntry{{Timestamp: parts[0], Log: parts[1]}}}
		}
	}
	if err := scanner.Err(); err != nil {
		return errors.New(err)
	}
	return nil
}

func formatPodlogs(podLogs io.ReadCloser, containerName string, logs []PodLogs, pod *v1.Pod) ([]PodLogs, error) {
	logChan := make(chan PodContainerLogs)
	errChan := make(chan error)

	go func() {
		err := formatContainerLogs(podLogs, containerName, logChan)
		if err != nil {
			errChan <- err
			return
		}
		close(errChan)
		close(logChan)
	}()

	for {
		select {
		case err := <-errChan:
			if err != nil {
				return nil, err
			}
		case containerLogs, ok := <-logChan:
			if !ok {
				return logs, nil
			}
			podFound := false
			for i, pl := range logs {
				if pl.Pod == pod.Name {
					logs[i].Containers = append(logs[i].Containers, containerLogs)
					podFound = true
					break
				}
			}
			if !podFound {
				logs = append(logs, PodLogs{
					Pod:        pod.Name,
					Containers: []PodContainerLogs{containerLogs},
				})
			}
		}
	}

}
