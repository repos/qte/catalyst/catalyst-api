package kubernetes

import (
	"context"
	"io"
	"os"

	"github.com/go-errors/errors"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

// Wrapper around the K8s client to allow for mocking during testing

type KubeClientWrapper interface {
	InjectClient(client *kubernetes.Clientset)
	EnsureNamespace()
	GetPods(namespace string, options metav1.ListOptions) (*v1.PodList, error)
	StreamPodLogs(namespace string, podName string, options *v1.PodLogOptions) (io.ReadCloser, error)
}

type KubeWrapperImp struct {
	client *kubernetes.Clientset
}

func NewKubeWrapper(kubeConfig string) KubeClientWrapper {
	var config *rest.Config
	var err error
	if os.Getenv("K8S_USE_SERVICE_ACCOUNT") == "true" {
		config, err = rest.InClusterConfig()
	} else {
		config, err = clientcmd.BuildConfigFromFlags("", kubeConfig)
	}
	utils.HandleError(err)
	kubeClient, err := kubernetes.NewForConfig(config)
	utils.HandleError(err)

	kubeClientWrapper := &KubeWrapperImp{}
	kubeClientWrapper.InjectClient(kubeClient)
	kubeClientWrapper.EnsureNamespace()

	return kubeClientWrapper
}

func (wi *KubeWrapperImp) InjectClient(client *kubernetes.Clientset) {
	if wi.client == nil {
		wi.client = client
	}
}

func (wi *KubeWrapperImp) EnsureNamespace() {
	_, err := wi.client.CoreV1().Namespaces().Get(context.TODO(), namespace, metav1.GetOptions{})
	if err != nil {
		nsSpec := &v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: namespace}}
		_, err = wi.client.CoreV1().Namespaces().Create(context.TODO(), nsSpec, metav1.CreateOptions{})
		if err != nil {
			utils.HandleError(err)
		}
	}
}

func (wi *KubeWrapperImp) StreamPodLogs(
	namespace string,
	podName string,
	options *v1.PodLogOptions,
) (io.ReadCloser, error) {
	stream, err := wi.client.CoreV1().Pods(namespace).GetLogs(podName, options).Stream(context.Background())
	if err != nil {
		return nil, errors.New(err)
	}
	return stream, nil
}

func (wi *KubeWrapperImp) GetPods(namespace string, options metav1.ListOptions) (*v1.PodList, error) {
	pods, err := wi.client.CoreV1().Pods(namespace).List(context.TODO(), options)
	if err != nil {
		return nil, errors.New(err)
	}
	return pods, nil
}
