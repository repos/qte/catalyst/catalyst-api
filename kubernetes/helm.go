package kubernetes

import (
	"log"
	"strings"

	"github.com/go-errors/errors"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/model"
	"helm.sh/helm/v3/pkg/release"
	"k8s.io/apimachinery/pkg/runtime"
)

type HelmClient struct {
	helmWrapper HelmClientWrapper
	knownCharts KnownCharts
}

func NewHelmClient(
	helmWrapper HelmClientWrapper,
	knownCharts KnownCharts,
) *HelmClient {
	helmClient := &HelmClient{
		helmWrapper: helmWrapper,
		knownCharts: knownCharts,
	}
	return helmClient
}

func (hc *HelmClient) GetRelease(relName string, getResourceInfo bool) (*release.Release, error) {
	return hc.helmWrapper.GetRelease(relName, getResourceInfo)
}

// GetDeployments Use utils.Get to extract values from the returned nested structures
func (hc *HelmClient) GetDeployments(relName string) ([]any, error) {
	var depsAsStruct []any
	deps, err := hc.getResources(relName, "deployment")
	if err != nil {
		return nil, err
	}

	for _, dep := range deps {
		depAsStruct := dep.(runtime.Unstructured).UnstructuredContent()
		depsAsStruct = append(depsAsStruct, depAsStruct)
	}
	return depsAsStruct, nil
}

func (hc *HelmClient) DeployChart(env *model.Environment) error {
	if env.Chart == nil {
		chartConfig := hc.knownCharts.ByName[env.ChartName]
		chrt, err := chartConfig.LoadInstance(true)
		if err != nil {
			return err
		}
		env.AddChart(chrt)
	}

	if hc.releaseExists(env.ReleaseName) {
		rel, err := hc.helmWrapper.UpgradeRelease(env.ReleaseName, env.Chart, env.Values())
		if err != nil {
			return err
		}
		log.Printf(`Updated release "%s" for chart "%s"`, rel.Name, env.ChartName)
	} else {
		rel, err := hc.helmWrapper.InstallRelease(env.ReleaseName, env.Chart, env.Values())
		if err != nil {
			return err
		}
		log.Printf(`Created release "%s" for chart "%s"`, rel.Name, env.ChartName)
	}

	return nil
}

func (hc *HelmClient) DeleteRelease(relName string) (*release.UninstallReleaseResponse, error) {
	_, err := hc.GetRelease(relName, true)
	if err != nil {
		return nil, err
	}
	uninstallRel, err := hc.helmWrapper.UninstallRelease(relName)
	if err != nil {
		return nil, err
	}
	return uninstallRel, nil
}

func (hc *HelmClient) getResources(relName string, resourceType string) ([]runtime.Object, error) {
	rel, err := hc.GetRelease(relName, true)
	if err != nil {
		return nil, err
	}

	for resType, resources := range rel.Info.Resources {
		if !strings.Contains(strings.ToLower(resType), resourceType) {
			continue
		}
		return resources, nil
	}

	return nil, errors.Errorf(`no resources of type "%s" found`, resourceType)
}

func (hc *HelmClient) releaseExists(relName string) bool {
	_, err := hc.GetRelease(relName, false)
	//TODO: Brittle, we should check the error
	return err == nil
}
