package auth

import (
	"fmt"
	"net/http"
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/bdd"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/bdd/mocks"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"
	"go.uber.org/mock/gomock"
)

var mc *gomock.Controller

func TestEnvs(t *testing.T) {
	RegisterFailHandler(Fail)
	mc = gomock.NewController(t)
	RunSpecs(t, "Authentication & Access Suite")
}

var _ = BeforeSuite(func() {
	bdd.DefaultSuiteSetup(mc)
})

var _ = Describe("Testing authentication & access control", Label("Auth"), func() {
	Context(`catalyst API is running`, func() {
		var res *http.Response
		var err error

		Describe("testing environment endpoints for different clients", func() {
			var env *bdd.Env
			defaultChecks := func(
				client bdd.Client,
				endpointCallUsing func(client bdd.Client) (*http.Response, error),
				prepareCallForEnv func(),
			) {
				By("owner should have access")
				if prepareCallForEnv != nil {
					prepareCallForEnv()
				}
				res, err = endpointCallUsing(client)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res).To(HaveHTTPStatus(http.StatusOK))

				By("unauthenticated calls should not be allowed")
				res, err = endpointCallUsing(bdd.NoAuth)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res).To(HaveHTTPStatus(http.StatusUnauthorized))

				By("other users should not have access")
				otherClient := bdd.User2
				if client == bdd.User2 {
					otherClient = bdd.User
				}
				res, err = endpointCallUsing(otherClient)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(res).To(HaveHTTPStatus(http.StatusForbidden))

				if client != bdd.Admin {
					By("admin should have access")
					if prepareCallForEnv != nil {
						prepareCallForEnv()
					}
					res, err = endpointCallUsing(bdd.Admin)
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(http.StatusOK))
				}
			}

			checkUnknown := func(
				client bdd.Client,
				endpointCallUsing func(client bdd.Client) (*http.Response, error),
			) {
				res, err = endpointCallUsing(client)
				Expect(err).ShouldNot(HaveOccurred())

				if client == bdd.Admin {
					By("admin should see env is missing")
					Expect(res).To(HaveHTTPStatus(http.StatusNotFound))
				} else {
					By("regular user should be denied access to missing env")
					Expect(res).To(HaveHTTPStatus(http.StatusForbidden))
				}
			}

			DescribeTable("POST /environments",
				func(client bdd.Client, expectedStatus int) {
					res, _ = bdd.NewEnvBuilder().CreateUsing(client)
					Expect(res).To(HaveHTTPStatus(expectedStatus))
				},
				Entry("when the client is an admin", bdd.Admin, http.StatusAccepted),
				Entry("when the client is a user", bdd.User, http.StatusAccepted),
				Entry("when the client is not authenticated", bdd.NoAuth, http.StatusUnauthorized),
			)

			DescribeTable("GET /environments",
				func(client bdd.Client, expectedStatus int) {
					res, err = bdd.Clients[client].Get("/environments")
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(expectedStatus))
				},
				Entry("when the client is an admin", bdd.Admin, http.StatusOK),
				Entry("when the client is a user", bdd.User, http.StatusOK),
				Entry("when the client is not authenticated", bdd.NoAuth, http.StatusUnauthorized),
			)

			DescribeTable("GET /environments/:id",
				func(client bdd.Client) {
					env = bdd.CreateDefaultEnvironment(client)
					defaultChecks(
						client,
						func(client bdd.Client) (*http.Response, error) {
							return bdd.Clients[client].Get(fmt.Sprintf("/environments/%d", env.Id))
						},
						nil,
					)
					checkUnknown(
						client,
						func(client bdd.Client) (*http.Response, error) {
							return bdd.Clients[client].Get(fmt.Sprintf("/environments/%d", env.Id+1000))
						},
					)
				},
				Entry("when the client is an admin", bdd.Admin),
				Entry("when the client is a user", bdd.User),
			)

			DescribeTable("GET /environments/:id/logs",
				func(client bdd.Client) {
					env = bdd.CreateDefaultEnvironment(client)
					defaultChecks(
						client,
						func(client bdd.Client) (*http.Response, error) {
							return bdd.Clients[client].Get(fmt.Sprintf("/environments/%d/logs", env.Id))
						},
						func() {
							mocks.MockPodLogs(env.Name, false)
						},
					)
					checkUnknown(
						client,
						func(client bdd.Client) (*http.Response, error) {
							return bdd.Clients[client].Get(fmt.Sprintf("/environments/%d/logs", env.Id+1000))
						},
					)
				},
				Entry("when the client is an admin", bdd.Admin),
				Entry("when the client is a user", bdd.User),
			)

			DescribeTable("DELETE /environments/:id",
				func(client bdd.Client) {
					defaultChecks(
						client,
						func(client bdd.Client) (*http.Response, error) {
							return bdd.Clients[client].Delete(fmt.Sprintf("/environments/%d", env.Id))
						},
						func() {
							env = bdd.CreateDefaultEnvironment(client)
							mocks.MockEnvDeletion(env.Name)
						},
					)

					By("owner should have access to deleted status")
					res, err = bdd.Clients[client].Get(fmt.Sprintf("/environments/%d", env.Id))
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(http.StatusGone))

					checkUnknown(
						client,
						func(client bdd.Client) (*http.Response, error) {
							return bdd.Clients[client].Delete(fmt.Sprintf("/environments/%d", env.Id+1000))
						},
					)
				},
				Entry("when the client is an admin", bdd.Admin),
				Entry("when the client is a user", bdd.User),
			)
		})

		Describe("testing apiToken endpoints for different clients", func() {
			DescribeTable("POST /apiTokens",
				func(client bdd.Client, expectedStatus int) {
					body := map[string]string{"description": "Test API Token"}
					res, err := bdd.Clients[client].Post("/apiTokens", body)
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(expectedStatus))
				},
				Entry("when the client is an admin", bdd.Admin, http.StatusCreated),
				Entry("when the client is a user", bdd.User, http.StatusForbidden),
				Entry("when the client is not authenticated", bdd.NoAuth, http.StatusUnauthorized),
			)

			DescribeTable("GET /apiTokens",
				func(client bdd.Client, expectedStatus int) {
					res, err := bdd.Clients[client].Get("/apiTokens")
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(expectedStatus))
				},
				Entry("when the client is an admin", bdd.Admin, http.StatusOK),
				Entry("when the client is a user", bdd.User, http.StatusForbidden),
				Entry("when the client is not authenticated", bdd.NoAuth, http.StatusUnauthorized),
			)

			DescribeTable("GET /apiTokens/:id",
				func(client bdd.Client, expectedStatus int) {
					By("creating a token for testing")
					body := map[string]string{"description": "Test API Token"}
					res, err := bdd.Clients[bdd.Admin].Post("/apiTokens", body)
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(http.StatusCreated))

					var tokenResponse map[string]interface{}
					bdd.ExtractBody(res, &tokenResponse)
					tokenID := fmt.Sprintf("%v", tokenResponse["id"])

					By("fetching the token by ID")
					res, err = bdd.Clients[client].Get("/apiTokens/" + tokenID)
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(expectedStatus))
				},
				Entry("when the client is an admin", bdd.Admin, http.StatusOK),
				Entry("when the client is a user", bdd.User, http.StatusForbidden),
				Entry("when the client is not authenticated", bdd.NoAuth, http.StatusUnauthorized),
			)

			DescribeTable("GET /apiTokens/:id with unknown ID",
				func(client bdd.Client, expectedStatus int) {
					body := map[string]string{"description": "Test API Token"}
					res, err := bdd.Clients[bdd.Admin].Post("/apiTokens", body)
					Expect(err).ShouldNot(HaveOccurred())
					var tokenResponse map[string]interface{}
					bdd.ExtractBody(res, &tokenResponse)

					tokenID, _ := utils.ParseUint(fmt.Sprintf("%v", tokenResponse["id"]))
					invalidID := fmt.Sprintf("%d", tokenID+1000)
					res, err = bdd.Clients[client].Get(fmt.Sprintf("/apiTokens/%s", invalidID))
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(expectedStatus))
				},
				Entry("when the client is an admin", bdd.Admin, http.StatusNotFound),
				Entry("when the client is a user", bdd.User, http.StatusForbidden),
				Entry("when the client is not authenticated", bdd.NoAuth, http.StatusUnauthorized),
			)
		})
	})
})
