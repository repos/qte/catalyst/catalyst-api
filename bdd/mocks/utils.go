package mocks

import (
	"bytes"
	"io"
	"strings"
	"time"

	"github.com/go-errors/errors"
	"go.uber.org/mock/gomock"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/release"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
)

// TODO: A builder here for mocks similar to the one for envs in bdd/utils.go would be useful

type EnvMockConfig struct {
	EnvName         string
	CreationDelay   *int // Secs
	FailChartDeploy bool
}

func MockEnvCreation(config *EnvMockConfig) {
	if config == nil {
		return
	}

	HelmWrapperMock.EXPECT().GetRelease(
		EnvReleaseMatcher(config.EnvName),
		gomock.Any(),
	).Return(nil, errors.New("rel does not exist"))

	HelmWrapperMock.EXPECT().InstallRelease(
		EnvReleaseMatcher(config.EnvName),
		gomock.Cond(func(arg any) bool {
			if chrt, ok := arg.(*chart.Chart); ok {
				return chrt.Name() == "mediawiki"
			} else {
				return false
			}
		}),
		gomock.Cond(func(arg any) bool {
			if values, ok := arg.(map[string]any); ok {
				if mwCore, ok := values["mediawikiCore"].(map[string]any); ok {
					return mwCore["ingress"] == "test.catalyst-qte.wmcloud.org"
				} else {
					return false
				}
			} else {
				return false
			}
		}),
	).DoAndReturn(func(relName string, _ *chart.Chart, _ map[string]any) (*release.Release, error) {
		if config.FailChartDeploy {
			return nil, errors.Errorf(
				"Oh dang! Chart deploy for env %s failed (as expected)", config.EnvName,
			)
		}

		return &release.Release{Name: relName}, nil
	})

	if !config.FailChartDeploy {
		HelmWrapperMock.EXPECT().GetRelease(
			EnvReleaseMatcher(config.EnvName),
			true,
		).DoAndReturn(func(relName string, _ bool) (*release.Release, error) {
			return &release.Release{
					Name: relName,
					Info: &release.Info{
						Status: release.StatusDeployed,
					},
				},
				nil
		})

		HelmWrapperMock.EXPECT().GetRelease(
			EnvReleaseMatcher(config.EnvName),
			true,
		).DoAndReturn(func(relName string, _ bool) (*release.Release, error) {
			if config.CreationDelay != nil {
				time.Sleep(time.Duration(*config.CreationDelay) * time.Second)
			}

			return &release.Release{
					Name: relName,
					Info: &release.Info{
						Resources: map[string][]runtime.Object{
							"deployment": {&unstructured.Unstructured{
								Object: map[string]any{
									"status": map[string]any{
										"replicas":          1,
										"availableReplicas": 1,
									},
								},
							}},
						},
					},
				},
				nil
		})
	}
}

func MockPodLogs(envName string, failLogRetrieval bool) {
	HelmWrapperMock.EXPECT().GetRelease(
		EnvReleaseMatcher(envName),
		true,
	).DoAndReturn(func(relName string, _ bool) (*release.Release, error) {
		return &release.Release{
				Name: relName,
				Info: &release.Info{
					Resources: map[string][]runtime.Object{
						"deployment": {&unstructured.Unstructured{
							Object: map[string]any{
								"spec": map[string]any{
									"selector": map[string]any{
										"matchLabels": map[string]any{"pod": "selector"},
									},
								},
							},
						}},
					},
				},
			},
			nil
	})

	pod := v1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name: "logsPod",
		},
		Status: v1.PodStatus{
			ContainerStatuses: []v1.ContainerStatus{
				{
					Name:  "logsCont",
					Ready: true,
				},
			},
		},
		Spec: v1.PodSpec{
			Containers: []v1.Container{
				{
					Name: "logsCont",
				},
			},
		},
	}
	KubeWrapperMock.EXPECT().GetPods("cat-env", metav1.ListOptions{
		LabelSelector: "pod=selector",
	}).Return(
		&v1.PodList{
			Items: []v1.Pod{pod},
		},
		nil,
	)

	KubeWrapperMock.EXPECT().StreamPodLogs(
		"cat-env", "logsPod", gomock.Any(),
	).DoAndReturn(func(namespace string, podName string, options *v1.PodLogOptions) (io.ReadCloser, error) {
		if failLogRetrieval {
			return nil, errors.Errorf("Oh my! Could not get logs for pod %s (as expected)", podName)
		} else {
			return io.NopCloser(bytes.NewReader([]byte("timestamp text"))), nil
		}
	})
}

func MockEnvDeletion(envName string) {
	HelmWrapperMock.EXPECT().GetRelease(EnvReleaseMatcher(envName), gomock.Any()).
		Return(nil, nil)
	HelmWrapperMock.EXPECT().UninstallRelease(EnvReleaseMatcher(envName))
}

func EnvReleaseMatcher(envName string) gomock.Matcher {
	return gomock.Cond(func(arg any) bool {
		if releaseName, ok := arg.(string); ok {
			return strings.HasPrefix(releaseName, envName)
		} else {
			return false
		}
	})
}
