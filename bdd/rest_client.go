package bdd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

type Client int

const (
	NoAuth Client = iota
	Admin
	User
	User2
)

type RestClient struct {
	baseApiUrl string
	userToken  string
}

type Header struct {
	key   string
	value string
}

// NOTE: Putting the clients in a map instead of e.g. putting them in variables is important due to
// a subtle quirkness of Ginkgo. In our tests we use Ginkgo Tables
// (https://onsi.github.io/ginkgo/#table-specs). These tables are generated during Ginkgo's Tree
// Construction Phase, whereas the clients can only be initialized during the Run Phase. Putting a
// client in a variable and passing that var to the Table means it will necessarily be `nil` at
// that point and make the spec fail when used
var Clients = map[Client]*RestClient{}

func (rc *RestClient) Get(apiPath string, headers ...Header) (*http.Response, error) {
	req, _ := http.NewRequest("GET", rc.baseApiUrl+apiPath, nil)
	return rc.do(req, headers...)
}

func (rc *RestClient) Post(apiPath string, body any, headers ...Header) (*http.Response, error) {
	jsonBody, _ := json.Marshal(body)
	req, _ := http.NewRequest("POST", rc.baseApiUrl+apiPath, bytes.NewBuffer(jsonBody))
	req.Header.Set("Content-Type", "application/json")
	return rc.do(req, headers...)
}

func (rc *RestClient) Delete(apiPath string, headers ...Header) (*http.Response, error) {
	req, _ := http.NewRequest("DELETE", rc.baseApiUrl+apiPath, nil)
	return rc.do(req, headers...)
}
func ExtractBody(res *http.Response, buf any) {
	GinkgoHelper()

	resBody, err := io.ReadAll(res.Body)
	defer res.Body.Close()
	Expect(err).ShouldNot(HaveOccurred())
	if len(resBody) == 0 {
		return
	}

	err = json.Unmarshal(resBody, buf)
	Expect(err).ShouldNot(HaveOccurred())
}

func CreateUserToken() string {
	GinkgoHelper()

	res, err := Clients[Admin].Post(
		"/apiTokens",
		map[string]any{
			"description": fmt.Sprintf("BDD-UserToken-%d", GinkgoRandomSeed()),
		},
	)
	Expect(err).ShouldNot(HaveOccurred())
	Expect(res).To(HaveHTTPStatus(http.StatusCreated))

	var token map[string]any
	ExtractBody(res, &token)
	return token["token"].(string)
}

func setUpRestClients(runnerId int) {
	baseApiUrl := fmt.Sprintf("http://localhost:%d/api", GetApiPort(runnerId))
	Clients[NoAuth] = &RestClient{baseApiUrl: baseApiUrl}
	Clients[Admin] = &RestClient{
		baseApiUrl: baseApiUrl,
		userToken:  adminToken,
	}
	Clients[User] = &RestClient{
		baseApiUrl: baseApiUrl,
		userToken:  CreateUserToken(),
	}
	Clients[User2] = &RestClient{
		baseApiUrl: baseApiUrl,
		userToken:  CreateUserToken(),
	}
}

func (rc *RestClient) do(req *http.Request, headers ...Header) (*http.Response, error) {
	for _, h := range headers {
		req.Header.Set(h.key, h.value)
	}

	if rc.userToken != "" {
		req.Header.Set("Authorization", "ApiToken "+rc.userToken)
	}

	return http.DefaultClient.Do(req)
}
