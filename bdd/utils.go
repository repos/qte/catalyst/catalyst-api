package bdd

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/bdd/mocks"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

type Env struct {
	Id          int
	Name        string
	ReleaseName string
	Object      map[string]any
}

type EnvBuilder struct {
	*Env
	body               map[string]any
	waitForRunning     bool
	creationMockConfig *mocks.EnvMockConfig
}

func NewEnvBuilder() (b *EnvBuilder) {
	b = &EnvBuilder{
		Env:            new(Env),
		waitForRunning: true,
		creationMockConfig: &mocks.EnvMockConfig{
			FailChartDeploy: false,
		},
	}
	b.Name = "BDD-Env-" + UniqueId()
	b.creationMockConfig.EnvName = b.Name
	return
}

func (b *EnvBuilder) WithName(name string) *EnvBuilder {
	b.Name = name
	b.creationMockConfig.EnvName = b.Name
	return b
}

func (b *EnvBuilder) ShouldWaitForRunningState(wait bool) *EnvBuilder {
	b.waitForRunning = wait
	return b
}

func (b *EnvBuilder) SendBody(body map[string]any) *EnvBuilder {
	b.body = body
	return b
}

func (b *EnvBuilder) MockCallsNotExpected() *EnvBuilder {
	b.creationMockConfig = nil
	b.waitForRunning = false
	return b
}

func (b *EnvBuilder) ShouldFailWhenDeployingChart() *EnvBuilder {
	if b.creationMockConfig == nil {
		return b
	}

	b.creationMockConfig.FailChartDeploy = true
	b.waitForRunning = false
	return b
}

func (b *EnvBuilder) WithCreationDelay(delay *int) *EnvBuilder {
	if b.creationMockConfig == nil {
		return b
	}

	b.creationMockConfig.CreationDelay = delay
	return b
}

func (b *EnvBuilder) CreateUsing(client Client) (*http.Response, *Env) {
	rc := Clients[client]
	if client == NoAuth {
		b.MockCallsNotExpected()
	}

	if b.creationMockConfig != nil {
		mocks.MockEnvCreation(b.creationMockConfig)
	}

	if b.body == nil {
		b.body = map[string]any{
			"name":      b.Name,
			"chartName": "mediawiki",
			"values": map[string]any{
				"mediawikiCore": map[string]any{
					"ingress": "test.catalyst-qte.wmcloud.org",
				},
			},
		}
	}
	res, err := rc.Post("/environments", b.body)
	Expect(err).ShouldNot(HaveOccurred())
	ExtractBody(res, &b.Object)
	if b.Object != nil {
		_, isEnvBody := b.Object["id"]
		if isEnvBody {
			b.Id = int(b.Object["id"].(float64))
			b.ReleaseName = b.Object["releaseName"].(string)
		}
	}

	if b.waitForRunning {
		// Make sure to consume mock expectations
		WaitForEnvRunning(rc, b.Env)
	}

	return res, b.Env
}

func CreateDefaultEnvironment(client Client) (env *Env) {
	res, env := NewEnvBuilder().CreateUsing(client)
	Expect(res).To(HaveHTTPStatus(http.StatusAccepted))
	return
}

func WaitForEnvRunning(rc *RestClient, env *Env) {
	GinkgoHelper()

	Eventually(func() string {
		res, err := rc.Get(fmt.Sprintf("/environments/%d", env.Id))
		Expect(err).ShouldNot(HaveOccurred())

		ExtractBody(res, &env.Object)
		if env.Object != nil && env.Object["status"] != nil {
			return env.Object["status"].(string)
		} else {
			return ""
		}
	}).
		WithTimeout(10 * time.Second).
		WithPolling(2 * time.Second).
		Should(Equal("running"))
}

func UniqueId() string {
	return fmt.Sprintf("%d", GinkgoRandomSeed()+time.Now().UnixNano())
}
