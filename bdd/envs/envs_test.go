package envs

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"net/http"
	"testing"
	"time"

	"github.com/go-errors/errors"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/bdd"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/bdd/mocks"
	"go.uber.org/mock/gomock"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var mc *gomock.Controller

func TestEnvs(t *testing.T) {
	RegisterFailHandler(Fail)
	mc = gomock.NewController(t)
	RunSpecs(t, "Environments Suite")
}

var _ = BeforeSuite(func() {
	bdd.DefaultSuiteSetup(mc)
})

var _ = Describe("Testing environments functionality", Label("Envs"), func() {
	Context(`catalyst API is running`, func() {

		DescribeTableSubtree(
			"testing environment endpoints for different clients",
			func(client bdd.Client) {
				var rc *bdd.RestClient
				var res *http.Response
				var err error
				var env *bdd.Env

				BeforeEach(func() {
					rc = bdd.Clients[client]
					env = bdd.CreateDefaultEnvironment(client)
				})

				It("POST /environments", func() {
					res, env = bdd.NewEnvBuilder().ShouldWaitForRunningState(false).CreateUsing(client)
					Expect(res).To(HaveHTTPStatus(http.StatusAccepted))

					By("should return the details for the newly created environment")
					Expect(env.Object).To(SatisfyAll(
						HaveKeyWithValue("name", env.Name),
						HaveKeyWithValue("chartName", "mediawiki"),
						HaveKeyWithValue("status", "starting"),
						HaveKey("values"),
					))
					Expect(env.Object["values"]).To(HaveKeyWithValue(
						"mediawikiCore",
						map[string]any{"ingress": "test.catalyst-qte.wmcloud.org"},
					))

					By("eventually the environment is running")
					bdd.WaitForEnvRunning(rc, env)
				})

				It("POST /environments should fail with", func() {
					By("400 when the environment name is already in use")
					res, _ := bdd.NewEnvBuilder().WithName(env.Name).MockCallsNotExpected().
						CreateUsing(client)
					Expect(res).To(HaveHTTPStatus(http.StatusBadRequest))

					By("400 when the environment being sent is invalid")
					res, _ = bdd.NewEnvBuilder().SendBody(
						map[string]any{"oopsie": "daisy"},
					).MockCallsNotExpected().CreateUsing(client)
					Expect(res).To(HaveHTTPStatus(http.StatusBadRequest))

					By("422 when the chart values being sent are invalid")
					res, _ = bdd.NewEnvBuilder().SendBody(
						map[string]any{
							"name":      "LeEnv",
							"chartName": "mediawiki",
							"values": map[string]any{
								"mediawikiCore": map[string]any{
									"fiddle": "sticks",
								},
							},
						},
					).MockCallsNotExpected().CreateUsing(client)
					Expect(res).To(HaveHTTPStatus(http.StatusUnprocessableEntity))

					By("500 when the chart deployment fails")
					res, _ = bdd.NewEnvBuilder().ShouldFailWhenDeployingChart().CreateUsing(client)
					Expect(res).To(HaveHTTPStatus(http.StatusInternalServerError))
				})

				It("GET /environments/:id", func() {
					By("should return the correct details for the environment")
					res, err = rc.Get(fmt.Sprintf("/environments/%d", env.Id))
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(http.StatusOK))

					bdd.ExtractBody(res, &env)
					Expect(env.Object).To(SatisfyAll(
						HaveKeyWithValue("name", env.Name),
						HaveKeyWithValue("chartName", "mediawiki"),
						Or(
							HaveKeyWithValue("status", "starting"),
							HaveKeyWithValue("status", "running"),
						),
						HaveKey("values"),
					))
					Expect(env.Object["values"]).To(HaveKeyWithValue(
						"mediawikiCore",
						map[string]any{"ingress": "test.catalyst-qte.wmcloud.org"},
					))
				})

				It("GET /environments/:id/logs", func() {
					By("should return the expected pod logs")
					mocks.MockPodLogs(env.Name, false)

					res, err = rc.Get(fmt.Sprintf("/environments/%d/logs", env.Id))
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(http.StatusOK))

					var rlogs []map[string]any
					bdd.ExtractBody(res, &rlogs)
					Expect(len(rlogs)).To(BeNumerically("==", 1))
					Expect(rlogs[0]).To(HaveKeyWithValue("pod", "logsPod"))
				})

				It("GET /environments/:id/logs should fail with", func() {
					By("500 when the logs cannot be retrieved from the cluster")
					mocks.MockPodLogs(env.Name, true)
					res, err = rc.Get(fmt.Sprintf("/environments/%d/logs", env.Id))
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(http.StatusInternalServerError))
				})

				It("GET /environments/:id/logs?stream=logsPod/logsCont", func() {
					doRequest := func() {
						res, err := rc.Get(fmt.Sprintf("/environments/%d/logs?stream=logsPod/logsCont", env.Id))
						Expect(err).ShouldNot(HaveOccurred())
						defer res.Body.Close()
						scanner := bufio.NewScanner(res.Body)
						Eventually(func() string {
							scanner.Scan()
							line := scanner.Text()
							return line
						}).
							WithTimeout(10 * time.Second).
							WithPolling(2 * time.Second).
							Should(Equal("data:{\"name\":\"logsCont\",\"logs\":[{\"timestamp\":\"timestamp\",\"log\":\"text\"}]}"))
					}

					By("eventually all logs are streamed")
					mockPodLogsStream(env.ReleaseName, false, 1, false)
					doRequest()

					By("logs from terminated containers are also returned")
					mockPodLogsStream(env.ReleaseName, true, 1, false)
					doRequest()
				})

				It("GET /environments/:id/logs?stream=logsPod/logsCont should fail with", func() {
					By("500 when the logs cannot be retrieved from the cluster")
					mockPodLogsStream(env.ReleaseName, false, 1, true)
					res, err := rc.Get(fmt.Sprintf("/environments/%d/logs?stream=logsPod/logsCont", env.Id))
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(http.StatusInternalServerError))
				})

				It("GET /environments/:id/logs?stream=logsPod/logsCont&stream=logsPod/logsCont2", func() {
					By("logs from multiple containers are streamed")
					mockPodLogsStream(env.ReleaseName, false, 2, false)
					res, err := rc.Get(fmt.Sprintf(
						"/environments/%d/logs?stream=logsPod/logsCont&stream=logsPod/logsCont2",
						env.Id,
					))
					Expect(err).ShouldNot(HaveOccurred())
					defer res.Body.Close()
					scanner := bufio.NewScanner(res.Body)
					var logs []string
					Eventually(func() []string {
						scanner.Scan()
						logs = append(logs, scanner.Text())
						return logs
					}).
						WithTimeout(10 * time.Second).
						WithPolling(2 * time.Second).
						Should(ContainElements(
							"data:{\"name\":\"logsCont\",\"logs\":[{\"timestamp\":\"timestamp\",\"log\":\"text\"}]}",
							"data:{\"name\":\"logsCont2\",\"logs\":[{\"timestamp\":\"timestamp\",\"log\":\"text\"}]}",
						))
				})

				It("GET /environments", func() {
					envAdmin := bdd.CreateDefaultEnvironment(bdd.Admin)
					envUser2 := bdd.CreateDefaultEnvironment(bdd.User2)

					switch client {
					case bdd.User:
						By("user should see only their own environments")
						res, err = rc.Get("/environments")
						Expect(err).ShouldNot(HaveOccurred())
						Expect(res).To(HaveHTTPStatus(http.StatusOK))

						var envs []map[string]any
						bdd.ExtractBody(res, &envs)
						// Note we don't clean the DB between tests, so we can see more than one env
						// here
						Expect(len(envs)).To(BeNumerically(">=", 1))
						var envIds []int
						for _, resEnv := range envs {
							envIds = append(envIds, int(resEnv["id"].(float64)))
						}
						Expect(envIds).To(ContainElement(env.Id))
						Expect(envIds).To(Not(ContainElements(envAdmin.Id, envUser2.Id)))
					case bdd.Admin:
						By("admin should see all environments")
						res, err = rc.Get("/environments")
						Expect(err).ShouldNot(HaveOccurred())
						Expect(res).To(HaveHTTPStatus(http.StatusOK))

						var envs []map[string]any
						bdd.ExtractBody(res, &envs)
						// Note we don't clean the DB between tests, so we can see more than three envs
						// here
						Expect(len(envs)).To(BeNumerically(">=", 3))
						var envIds []int
						for _, resEnv := range envs {
							envIds = append(envIds, int(resEnv["id"].(float64)))
						}
						Expect(envIds).To(ContainElements(env.Id, envAdmin.Id, envUser2.Id))
					default:
						Fail("Unexpected client. This should not have happened")
					}
				})

				It("DELETE /environments/:id", func() {
					By("should delete the environment")
					mocks.MockEnvDeletion(env.Name)

					res, err = rc.Delete(fmt.Sprintf("/environments/%d", env.Id))
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(http.StatusOK))

					res, err = rc.Get(fmt.Sprintf("/environments/%d", env.Id))
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(http.StatusGone))
				})

				// T379366
				It("DELETE /environments/:id during env creation", func() {
					By("should succeed even during env creation")
					delay := 3
					res, env = bdd.NewEnvBuilder().ShouldWaitForRunningState(false).
						WithCreationDelay(&delay).CreateUsing(client)
					Expect(res).To(HaveHTTPStatus(http.StatusAccepted))

					mocks.MockEnvDeletion(env.Name)
					res, err = rc.Delete(fmt.Sprintf("/environments/%d", env.Id))
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(http.StatusOK))

					By("env should remain deleted")
					time.Sleep(3 * time.Second)
					res, err = rc.Get(fmt.Sprintf("/environments/%d", env.Id))
					Expect(err).ShouldNot(HaveOccurred())
					Expect(res).To(HaveHTTPStatus(http.StatusGone))
				})
			},
			Entry("when the client is an admin", bdd.Admin),
			Entry("when the client is a user", bdd.User),
		)
	})
})

func mockPodLogsStream(releaseName string, isTerminated bool, numContainers int, failLogRetrieval bool) {
	var startedStatus = true
	var terminatedState v1.ContainerStateTerminated
	if isTerminated {
		terminatedState = v1.ContainerStateTerminated{}
	}

	pod := v1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name: "logsPod",
		},
		Status: v1.PodStatus{
			ContainerStatuses: []v1.ContainerStatus{
				{
					Name:    "logsCont",
					Ready:   true,
					Started: &startedStatus,
					State: v1.ContainerState{
						Terminated: &terminatedState,
					},
				},
				{
					Name:    "logsCont2",
					Ready:   true,
					Started: &startedStatus,
					State: v1.ContainerState{
						Terminated: &terminatedState,
					},
				},
			},
		},
		Spec: v1.PodSpec{
			Containers: []v1.Container{
				{
					Name: "logsCont",
				},
				{
					Name: "logsCont2",
				},
			},
		},
	}
	mocks.KubeWrapperMock.EXPECT().GetPods("cat-env", metav1.ListOptions{
		LabelSelector: "app=" + releaseName + "-logsPod",
	}).Return(
		&v1.PodList{
			Items: []v1.Pod{pod},
		},
		nil,
	).Times(numContainers)

	mocks.KubeWrapperMock.EXPECT().StreamPodLogs(
		"cat-env", "logsPod", gomock.Any(),
	).DoAndReturn(func(namespace, podName string, options *v1.PodLogOptions) (io.ReadCloser, error) {
		if failLogRetrieval {
			return nil, errors.Errorf("Oh rats! Could not get logs for pod %s (as expected)", podName)
		} else {
			return io.NopCloser(bytes.NewReader([]byte("timestamp text"))), nil
		}
	}).Times(numContainers)
}
