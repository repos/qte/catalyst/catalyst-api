package charts

import (
	"net/http"
	"os"
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/bdd"
	"go.uber.org/mock/gomock"
)

var mc *gomock.Controller

func TestCharts(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Charts Suite")
}

var _ = BeforeSuite(func() {
	bdd.StartUpApi(GinkgoParallelProcess(), nil, nil, func() {
		os.Setenv("CHARTS_CONFIG_LOCATION", "charts.yaml")
	})
})

var _ = Describe("Testing charts with no auth", Label("Charts"), func() {
	Context(`catalyst API is aware of charts at "bdd/charts/charts.yaml"`, func() {
		var res *http.Response
		var err error
		Describe("we retrieve all charts", func() {
			BeforeEach(func() {
				res, err = bdd.Clients[bdd.NoAuth].Get("/charts")
				Expect(err).ShouldNot(HaveOccurred())
			})
			It(`should return charts in "bdd/charts/charts.yaml"`, func() {
				Expect(res).To(HaveHTTPStatus(http.StatusOK))
				Expect(res).To(HaveHTTPBody(MatchJSON(`
					[
						{
							"name": "mediawiki",
							"description": "mediawiki and extensions",
							"gitRepo": "https://gitlab.wikimedia.org/repos/test-platform/catalyst/ci-charts",
							"directory": "mediawiki"
						},
						{
							"name": "someChart",
							"description": "a chart and then some",
							"gitRepo": "https://gitlab.wikimedia.org/repos/test-platform/catalyst/nopes",
							"directory": ""
						}
					]
				`)))
			})
		})

		Describe("we retrieve a known chart", func() {
			BeforeEach(func() {
				res, err = bdd.Clients[bdd.NoAuth].Get("/charts/mediawiki")
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should have returned the chart and its default values", func() {
				Expect(res).To(HaveHTTPStatus(http.StatusOK))

				var chart map[string]any
				bdd.ExtractBody(res, &chart)
				Expect(chart).To(SatisfyAll(
					HaveKeyWithValue("name", "mediawiki"),
					HaveKeyWithValue("description", "mediawiki and extensions"),
					HaveKeyWithValue("gitRepo", "https://gitlab.wikimedia.org/repos/test-platform/catalyst/ci-charts"),
					HaveKeyWithValue("directory", "mediawiki"),
					HaveKey("defaultValues"),
				))
				defValues := chart["defaultValues"]
				Expect(defValues).To(SatisfyAll(
					HaveKey("mediawikiCore"),
					HaveKey("db"),
					HaveKey("extensions"),
					HaveKey("skins"),
				))
			})
		})

		Describe("we retrieve an unknown chart", func() {
			BeforeEach(func() {
				res, err = bdd.Clients[bdd.NoAuth].Get("/charts/inconnu")
				Expect(err).ShouldNot(HaveOccurred())
			})
			It("should return a descriptive error message", func() {
				Expect(res).To(HaveHTTPStatus(http.StatusNotFound))
				Expect(res).To(HaveHTTPBody(MatchJSON(`
					{
						"statusCode": 404,
						"statusText": "Not Found",
						"details": [
							"unknown chart: inconnu"
						]
					}
				`)))
			})
		})
	})
})
