package bdd

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/kubernetes"

	"go.uber.org/mock/gomock"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/bdd/mocks"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/setup"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"
)

const dbTimeout = time.Second * 15
const adminToken = "0-550e8400-e29b-41d4-a716-446655440000"
const defaultChartLocation = "../../charts.yaml"

func DefaultSuiteSetup(mc *gomock.Controller) {
	GinkgoHelper()

	mocks.HelmWrapperMock = mocks.NewMockHelmClientWrapper(mc)
	mocks.KubeWrapperMock = mocks.NewMockKubeClientWrapper(mc)
	StartUpApi(GinkgoParallelProcess(), mocks.HelmWrapperMock, mocks.KubeWrapperMock, func() {})
}

func StartUpApi(
	runnerId int,
	helmWrapperMock *mocks.MockHelmClientWrapper,
	kubeWrapperMock *mocks.MockKubeClientWrapper,
	setUpSuite func(),
) {
	GinkgoHelper()

	setUpEnv()
	setUpSuite()
	// The DB needs to be provided externally
	waitForDB()
	startUpCatalyst(runnerId, helmWrapperMock, kubeWrapperMock)
	setUpRestClients(runnerId)
}

func GetApiPort(runnerId int) int {
	return 8080 + runnerId
}

func setUpEnv() {
	os.Setenv("ADMIN_TOKEN", adminToken)
	os.Setenv("CHARTS_CONFIG_LOCATION", defaultChartLocation)
	os.Setenv("DB_USER", utils.GetEnv("TEST_DB_USER", "root"))
	os.Setenv("DB_PASS", utils.GetEnv("TEST_DB_PASS", "supersecret"))
	os.Setenv("DB_HOST", utils.GetEnv("TEST_DB_HOST", "localhost"))
	os.Setenv("DB_PORT", utils.GetEnv("TEST_DB_PORT", "3306"))
	os.Setenv("DB_DATABASE", utils.GetEnv("TEST_DB_DATABASE", "catalyst"))
}

func waitForDB() {
	Eventually(func() string {
		dbUrl := fmt.Sprintf("http://%s:%s", os.Getenv("DB_HOST"), os.Getenv("DB_PORT"))
		_, err := http.Get(dbUrl)
		return err.Error()
	}).WithTimeout(dbTimeout).WithPolling(2 * time.Second).
		ShouldNot(ContainSubstring("connection reset by peer"))
}

func startUpCatalyst(runnerId int,
	helmWrapperMock kubernetes.HelmClientWrapper,
	kubeWrapperMock kubernetes.KubeClientWrapper,
) {
	s := &setup.Singletons{}
	var helmClientProvider setup.BackendDepProvider = func(s *setup.Singletons) any {
		return kubernetes.NewHelmClient(helmWrapperMock, s.KnownCharts)
	}
	var kubernetesClientProvider setup.BackendDepProvider = func(_ *setup.Singletons) any {
		return kubernetes.NewKubernetesClient(kubeWrapperMock)
	}
	setup.WireUpCatalyst(
		s,
		setup.BackendDependencies{
			Helm: helmClientProvider,
			K8s:  kubernetesClientProvider,
		},
	)

	apiPort := GetApiPort(runnerId)
	go s.Router.RunRouter(fmt.Sprintf("0.0.0.0:%d", apiPort))

	// Wait for Catalyst to come online
	Eventually(func() int {
		if res, err := http.Get(fmt.Sprintf("http://localhost:%d/healthz", apiPort)); err == nil {
			return res.StatusCode
		} else {
			return -1
		}
	}).WithTimeout(2 * time.Second).WithPolling(500 * time.Millisecond).Should(Equal(http.StatusOK))
}
