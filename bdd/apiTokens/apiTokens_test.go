package apiTokens

import (
	"fmt"
	"net/http"
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/bdd"
	"go.uber.org/mock/gomock"
)

var mc *gomock.Controller

func TestTokens(t *testing.T) {
	RegisterFailHandler(Fail)
	mc = gomock.NewController(t)
	RunSpecs(t, "Tokens Suite")
}

var _ = BeforeSuite(func() {
	bdd.DefaultSuiteSetup(mc)
})

var _ = Describe("Testing tokens functionality", Label("Tokens"), func() {
	Context(`catalyst API is running`, func() {
		var rc *bdd.RestClient

		BeforeEach(func() {
			rc = bdd.Clients[bdd.Admin]
		})

		It("POST /apiToken", func() {
			body := map[string]string{"description": "Test API Token"}
			res, _ := bdd.Clients[bdd.Admin].Post("/apiTokens", body)
			var tokenresponse map[string]any
			bdd.ExtractBody(res, &tokenresponse)
			By("should return the details for the newly created token")
			Expect(tokenresponse).To(SatisfyAll(
				HaveKeyWithValue("description", "Test API Token"),
			))
		})

		It("POST /apiToken should fail with", func() {
			By("400 when the /apiTokens request is improperly formed")
			res, _ := bdd.Clients[bdd.Admin].Post("/apiTokens", "invalid input")
			Expect(res).To(HaveHTTPStatus(http.StatusBadRequest))
		})

		It("GET /apiTokens", func() {
			tokenBodies := []map[string]string{
				{"description": "Test API Token1"},
				{"description": "Test API Token2"},
				{"description": "Test API Token3"},
			}

			for _, body := range tokenBodies {
				_, _ = bdd.Clients[bdd.Admin].Post("/apiTokens", body)
			}
			res, err := rc.Get("/apiTokens")
			Expect(err).ShouldNot(HaveOccurred())
			var tokenResponse []map[string]any
			bdd.ExtractBody(res, &tokenResponse)
			// Note we don't clean the DB between tests, so we can see more than three tokens
			By("admin should see all apiTokens")
			Expect(len(tokenResponse)).To(BeNumerically(">=", 3))

			By("newly tokens created via Post should also be retrieved by Get")
			Expect(tokenResponse).To(ContainElements(
				HaveKeyWithValue("description", "Test API Token1"),
				HaveKeyWithValue("description", "Test API Token2"),
				HaveKeyWithValue("description", "Test API Token3"),
			))
		})

		It("GET /apiTokens/:id", func() {
			By("should be able to get apiTokens by id")
			body := map[string]string{"description": "Test API Token4"}
			res, _ := bdd.Clients[bdd.Admin].Post("/apiTokens", body)

			var tokenResponse map[string]interface{}
			bdd.ExtractBody(res, &tokenResponse)
			tokenID := fmt.Sprintf("%v", tokenResponse["id"])

			res, err := rc.Get("/apiTokens/" + tokenID)
			Expect(err).ShouldNot(HaveOccurred())
			var singleTokenResponse map[string]any
			bdd.ExtractBody(res, &singleTokenResponse)
			By("the token retrieved by ID should match the description")
			Expect(singleTokenResponse).To(HaveKeyWithValue("description", "Test API Token4"))
		})
	})

})
