package setup

import (
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/crypto"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/kubernetes"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/persistence"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/rest"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/service"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"
)

type Singletons struct {
	Repository       *persistence.Repository
	KnownCharts      kubernetes.KnownCharts
	HashFunction     crypto.Hash
	HelmClient       *kubernetes.HelmClient
	KubernetesClient *kubernetes.KubernetesClient
	EnvService       *service.EnvironmentService
	Catalyst         *service.Catalyst
	Auth             *rest.Auth
	Access           *rest.Access
	Router           *rest.Router
}

type BackendDependencies struct {
	Helm BackendDepProvider
	K8s  BackendDepProvider
}

type BackendDepProvider func(s *Singletons) any

func WireUpCatalyst(s *Singletons, bd BackendDependencies) {
	setUpPersistence(s)
	loadKnownCharts(s)
	s.HelmClient = bd.Helm(s).(*kubernetes.HelmClient)
	s.KubernetesClient = bd.K8s(s).(*kubernetes.KubernetesClient)
	s.EnvService = service.NewEnvironmentService(
		s.Repository,
		s.HelmClient,
		s.KnownCharts,
	)
	setUpCrypto(s)
	s.Catalyst = service.NewCatalyst(
		s.Repository,
		s.HelmClient,
		s.KubernetesClient,
		s.HashFunction,
		s.KnownCharts,
		s.EnvService,
	)
	setUpAuth(s)
	s.Access = rest.NewAccess(s.Catalyst)
	s.Router = rest.NewRouter(
		s.KnownCharts,
		s.HashFunction,
		s.Auth,
		s.Access,
		s.EnvService,
		s.Catalyst,
	)
}

func setUpPersistence(s *Singletons) {
	dbCon, err := persistence.GetMysqlDBConnection()
	utils.HandleError(err)
	s.Repository, err = persistence.NewRepository(dbCon)
	utils.HandleError(err)
}

func loadKnownCharts(s *Singletons) {
	s.KnownCharts = kubernetes.KnownCharts{}
	err := s.KnownCharts.LoadConfigs()
	utils.HandleError(err)
}

func setUpCrypto(s *Singletons) {
	saltF := crypto.RandSalt{}
	s.HashFunction = crypto.DefaultArgon2idHash(saltF)
}

func setUpAuth(s *Singletons) {
	s.Auth = rest.NewAuth(s.HashFunction, s.Catalyst)
	rest.VerifyAdminToken()
}
