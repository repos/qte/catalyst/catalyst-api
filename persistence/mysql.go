package persistence

import (
	"os"

	"github.com/go-errors/errors"

	"github.com/go-sql-driver/mysql"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"
	gmysql "gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

func GetMysqlDBConnection() (*gorm.DB, error) {
	username := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASS")
	host := utils.GetEnv("DB_HOST", "localhost")
	port := utils.GetEnv("DB_PORT", "3306")
	dbName := utils.GetEnv("DB_DATABASE", "catalyst")
	dsParams := "?charset=utf8mb4&parseTime=True&loc=Local"
	dsn := username + ":" + password + "@tcp(" + host + ":" + port + ")/" + dbName + dsParams
	db, err := gorm.Open(gmysql.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})
	if err != nil {
		return nil, err
	}

	return db, nil
}

func IsDuplicateEntry(err error) bool {
	return getMySQLErrorCode(err) == 1062
}

func getMySQLErrorCode(err error) uint16 {
	if werr, ok := err.(*errors.Error); ok {
		err = errors.Unwrap(werr)
	}
	return err.(*mysql.MySQLError).Number
}
