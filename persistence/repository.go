package persistence

import (
	"github.com/go-errors/errors"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/model"
	"gorm.io/gorm"
)

type Persistable interface {
	Persistable()
}

type Repository struct {
	*gorm.DB
}

func NewRepository(db *gorm.DB) (*Repository, error) {
	repo := Repository{db}
	// Create/migrate DB tables
	err := repo.AutoMigrate(
		&model.Environment{},
		&model.Token{},
	)
	if err != nil {
		return nil, err
	}
	return &repo, nil
}

// SaveModel Upsert-type function for model structs
func (rep *Repository) SaveModel(model Persistable) error {
	res := rep.Save(model)
	if res.Error != nil {
		return errors.New(res.Error)
	}
	return nil
}

func (rep *Repository) Get(obj Persistable, id uint) error {
	res := rep.First(&obj, id)
	if res.Error != nil {
		return errors.New(res.Error)
	}

	return nil
}

func (rep *Repository) DeleteDueToError(model Persistable, originalError error) error {
	err := rep.Delete(model).Error
	if err != nil {
		return errors.New(errors.Join(err, originalError))
	} else {
		return originalError
	}
}

func (rep *Repository) GetEnv(id uint) (*model.Environment, error) {
	var env model.Environment
	// unscoped queries include soft-deleted entries
	res := rep.Unscoped().First(&env, id)
	if res.Error != nil {
		return nil, errors.New(res.Error)
	}
	return &env, nil
}

func (rep *Repository) GetEnvs() (*[]model.Environment, error) {
	var envs []model.Environment
	res := rep.Find(&envs)
	if res.Error != nil {
		return nil, errors.New(res.Error)
	}
	return &envs, nil
}

func (rep *Repository) GetEnvsByOwner(ownerId uint) (*[]model.Environment, error) {
	var envs []model.Environment
	res := rep.Find(&envs, "owner_id", ownerId)
	if res.Error != nil {
		return nil, errors.New(res.Error)
	}
	return &envs, nil
}

// SoftDeleteEnv soft-delete when model has gorm.DeleteAt field
func (rep *Repository) SoftDeleteEnv(env *model.Environment) error {
	env.Status = model.EnvDeleted
	err := rep.SaveModel(env)
	if err != nil {
		return err
	}
	res := rep.Delete(env)
	if res.Error != nil {
		return errors.New(res.Error)
	}
	return nil
}

func (rep *Repository) WithTransaction(f func(tRep *Repository) error) error {
	return rep.Transaction(func(tx *gorm.DB) error {
		tRep := Repository{tx}
		return f(&tRep)
	})
}
