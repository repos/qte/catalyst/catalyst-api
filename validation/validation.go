package validation

import (
	"github.com/gin-gonic/gin/binding"
	"github.com/go-errors/errors"
	"github.com/go-playground/validator/v10"
	"github.com/santhosh-tekuri/jsonschema/v5"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/catalystError"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/kubernetes"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/model"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/service"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"
)

const KnownChart = "knownchart"

type customErrorFunc func(validator.FieldError) error

var CustomErrors = map[string]customErrorFunc{
	KnownChart: getUnknownChartError,
}

type CustomValidations struct {
	ctx Context
}

type Context struct {
	KnownCharts kubernetes.KnownCharts
	EnvService  *service.EnvironmentService
}

func SetupCustomValidations(ctx Context) *CustomValidations {
	cv := CustomValidations{ctx: ctx}
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		utils.HandleError(v.RegisterValidation(KnownChart, cv.knownChart))
	}
	return &cv
}

func (cv *CustomValidations) ChartIsKnown(chartName string) bool {
	_, ok := cv.ctx.KnownCharts.ByName[chartName]
	return ok
}

func (cv *CustomValidations) ValidateEnvParameters(env *model.Environment) (bool, error) {
	chartConfig := cv.ctx.KnownCharts.ByName[env.ChartName]
	chrt, err := chartConfig.LoadInstance(true)
	if err != nil {
		return false, err
	}
	env.AddChart(chrt)

	if env.Chart.Schema == nil {
		return true, nil
	}

	sch, err := jsonschema.CompileString(env.Chart.Name()+".sch.json", string(env.Chart.Schema))
	if err != nil {
		ics := catalystError.InvalidChartSchema{Err: errors.New(err)}
		return false, ics
	}

	if err = sch.Validate(env.Values()); err != nil {
		iev := catalystError.InvalidEnvValues{Err: errors.New(err)}
		return false, iev
	}

	return true, nil
}

func (cv *CustomValidations) knownChart(fl validator.FieldLevel) bool {
	chartName := fl.Field().String()
	return cv.ChartIsKnown(chartName)
}

func getUnknownChartError(valErr validator.FieldError) error {
	return errors.Errorf("unknown chart '%s'", valErr.Value())
}
