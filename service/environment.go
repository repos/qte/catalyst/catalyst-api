package service

import (
	"context"
	"log"
	"strconv"
	"time"

	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/catalystError"

	"github.com/go-errors/errors"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/kubernetes"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/model"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/persistence"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"
	"gorm.io/gorm/clause"
	"helm.sh/helm/v3/pkg/release"
)

// In minutes
var deploymentTimeout, _ = strconv.Atoi(utils.GetEnv("DEPLOYMENT_TIMEOUT", "10"))

type EnvironmentService struct {
	repo   *persistence.Repository
	hc     *kubernetes.HelmClient
	kchrts kubernetes.KnownCharts
}

func NewEnvironmentService(
	repo *persistence.Repository,
	hc *kubernetes.HelmClient,
	kchrts kubernetes.KnownCharts,
) *EnvironmentService {
	return &EnvironmentService{
		repo:   repo,
		hc:     hc,
		kchrts: kchrts,
	}
}

func (es *EnvironmentService) MarkAsFailed(env *model.Environment, originalError error) error {
	return MarkAsFailed(es.repo, env, originalError)
}

// MarkAsFailed Useful inside of transactions
func MarkAsFailed(repo *persistence.Repository, env *model.Environment, originalError error) error {
	env.Status = model.EnvFailed
	errUpd := repo.SaveModel(env)
	if errUpd != nil {
		return errors.New(errors.Join(errUpd, originalError))
	} else {
		return originalError
	}
}

// UpdateDeploymentStatusFor verifies that a K8s deployment succeeded. Performs active waiting so
// it should normally be called in its own goroutine
func (es *EnvironmentService) UpdateDeploymentStatusFor(env *model.Environment) {
	timeoutDuration := time.Duration(deploymentTimeout) * time.Minute
	timeout, cancel := context.WithTimeout(context.Background(), timeoutDuration)
	defer cancel()
	done := false
	for {
		// The transaction and lock handle the possibility of the env being deleted while we wait
		// for deployment to complete
		tErr := es.repo.WithTransaction(func(tRep *persistence.Repository) error {
			failDueTo := func(err error) error {
				done = true
				return MarkAsFailed(tRep, env, nil)
			}

			// Do not change this to a SHARE lock. It can lead to a subtle deadlock with the
			// env delete operation. See:
			// https://dev.mysql.com/doc/refman/5.7/en/innodb-deadlock-example.html
			// (old version but still applies and clearly explained)
			tRep.Clauses(clause.Locking{Strength: "UPDATE"}).Find(env)

			tEnv, err := tRep.GetEnv(env.Id)
			if err != nil {
				return failDueTo(err)
			}

			if tEnv.IsDeleted() {
				done = true
				return nil
			}

			rel, err := es.hc.GetRelease(env.ReleaseName, true)
			if err != nil {
				return failDueTo(err)
			}

			select {
			case <-timeout.Done():
				err = errors.Errorf("environment '%s' is not available after %d minutes",
					env.Name,
					int(timeoutDuration.Minutes()),
				)
				return failDueTo(err)
			default:
				if rel.Info.Status != release.StatusDeployed {
					err = errors.Errorf("deployment for environment '%s' failed. Status is '%v'",
						env.Name,
						rel.Info.Status,
					)
					return failDueTo(err)
				}

				// Even if the Helm release is marked as "deployed", that doesn't mean the deployment
				// succeeded. We verify the K8s deployments too
				if es.allK8sDeploymentsAreAvailable(rel.Name) {
					env.Status = model.EnvRunning
					err = tRep.SaveModel(env)
					if err != nil {
						return failDueTo(err)
					}
					done = true
				}
			}
			return nil
		})
		if tErr != nil {
			log.Println("Transaction canceled:\n", tErr.(catalystError.CatalystError).ErrorStack())
			done = true
		}

		if done {
			return
		}
		time.Sleep(2 * time.Second)
	}
}

func (es *EnvironmentService) allK8sDeploymentsAreAvailable(relName string) bool {
	deployments, err := es.hc.GetDeployments(relName)
	if err != nil {
		return false
	}

	for _, deployment := range deployments {
		replicas, errR := utils.Get(deployment, "status", "replicas")
		availReplicas, errA := utils.Get(deployment, "status", "availableReplicas")
		if errR != nil || errA != nil {
			return false
		}

		if replicas != availReplicas {
			return false
		}
	}

	return true
}
