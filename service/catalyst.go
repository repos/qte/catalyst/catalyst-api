package service

import (
	"fmt"

	"github.com/go-errors/errors"
	"github.com/google/uuid"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/catalystError"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/crypto"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/kubernetes"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/model"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/persistence"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"
	"gorm.io/gorm"
)

type Catalyst struct {
	repo  *persistence.Repository
	helm  *kubernetes.HelmClient
	k8s   *kubernetes.KubernetesClient
	hash  crypto.Hash
	chrts kubernetes.KnownCharts
	svc   *EnvironmentService
}

func NewCatalyst(
	repo *persistence.Repository,
	helm *kubernetes.HelmClient,
	k8s *kubernetes.KubernetesClient,
	hash crypto.Hash,
	chrts kubernetes.KnownCharts,
	svc *EnvironmentService,
) *Catalyst {
	return &Catalyst{
		repo:  repo,
		helm:  helm,
		k8s:   k8s,
		hash:  hash,
		chrts: chrts,
		svc:   svc,
	}
}

type StreamParams struct {
	AppName       string
	ContainerName string
}

func (c *Catalyst) GetAllEnvironments() (*[]model.Environment, error) {
	allEnvs, err := c.repo.GetEnvs()
	if err != nil {
		return nil, err
	}
	return allEnvs, nil
}

func (c *Catalyst) GetEnvironmentsByOwner(ownerId uint) (*[]model.Environment, error) {
	allEnvs, err := c.repo.GetEnvsByOwner(ownerId)
	if err != nil {
		return nil, err
	}
	return allEnvs, nil
}

func (c *Catalyst) GetEnvironment(id uint) (*model.Environment, error) {
	return c.GetEnvironmentDeleted(id, false)
}
func (c *Catalyst) GetEnvironmentDeleted(id uint, ifDeleted bool) (*model.Environment, error) {
	env, err := c.repo.GetEnv(id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			ue := catalystError.UnknownEntry{Err: errors.Errorf("environment %d does not exist", id)}
			return nil, ue
		}
		return nil, err
	}
	if env.IsDeleted() {
		de := catalystError.DeletedEntry{Err: errors.Errorf("environment %s has been deleted", env.Name)}
		if ifDeleted {
			return env, de
		}
		return nil, de
	}
	return env, nil
}

func (c *Catalyst) GetCharts() []kubernetes.ChartConfig {
	return c.chrts.Charts
}

func (c *Catalyst) GetChart(chartName string) (*kubernetes.ChartDetails, error) {
	chart := c.chrts.ByName[chartName]
	return chart.GetDetails()
}

// CreateEnvironmentAsync Function will deploy the environment to the target cluster, but will not wait to confirm that
// creation was successful
func (c *Catalyst) CreateEnvironmentAsync(env *model.Environment) error {
	env.Status = model.EnvStarting
	err := c.repo.SaveModel(env)
	if err != nil {
		if persistence.IsDuplicateEntry(err) {
			de := catalystError.DuplicateEntry{Err: errors.Errorf("environment %s already exists", env.Name)}
			return de
		} else {
			return err
		}
	}

	// At this point we have inserted the new env in the DB and `env.ID` has been populated. Which
	// means `env.ReleaseName` can now be set
	env.ReleaseName = fmt.Sprintf("%s-%d", env.Name, env.Id)
	err = c.repo.SaveModel(env)
	if err != nil {
		return err
	}

	err = c.helm.DeployChart(env)
	if err != nil {
		return c.svc.MarkAsFailed(env, err)
	}

	go c.svc.UpdateDeploymentStatusFor(env)

	return nil
}

func (c *Catalyst) DeleteEnvironment(id uint) error {
	env, err := c.GetEnvironment(id)
	if err != nil {
		return err
	}

	err = c.repo.SoftDeleteEnv(env)
	if err != nil {
		return err
	}

	_, err = c.helm.DeleteRelease(env.ReleaseName)
	if err != nil {
		delErr := errors.Errorf("failed to delete release %s for environment %s", env.ReleaseName, env.Name)
		return errors.New(errors.Join(delErr, err))
	}
	return nil
}

func (c *Catalyst) GetEnvironmentLogs(id uint) ([]kubernetes.PodLogs, error) {
	env, err := c.GetEnvironment(id)
	if err != nil {
		return nil, err
	}

	deployments, err := c.helm.GetDeployments(env.ReleaseName)
	if err != nil {
		return nil, err
	}

	var logs []kubernetes.PodLogs
	for _, deployment := range deployments {
		matchLabels, err := utils.Get(deployment, "spec", "selector", "matchLabels")
		if err != nil {
			return nil, err
		}

		var podsLabelSelector string
		for lk, lv := range matchLabels.(map[string]any) {
			podsLabelSelector = lk + "=" + lv.(string)
			break
		}

		pods, err := c.k8s.GetPods(podsLabelSelector)
		if err != nil {
			return nil, err
		}

		for _, pod := range pods.Items {
			pLogs, err := c.k8s.GetLogs(&pod)
			if err != nil {
				return nil, err
			}
			logs = append(logs, pLogs...)
		}
	}

	return logs, nil
}

func (c *Catalyst) StreamContainerLogs(
	id uint,
	containerNames []StreamParams,
	notifyError func(error),
	notifyEvent func(kubernetes.PodContainerLogs),
) {
	logsTerminated := 0
	numContainers := 0
	errChan := make(chan error)
	logChan := make(chan kubernetes.PodContainerLogs)
	termChan := make(chan int)

	env, err := c.GetEnvironment(id)
	if err != nil {
		notifyError(err)
		return
	}

	for _, name := range containerNames {
		pods, err := c.k8s.GetPods("app=" + env.ReleaseName + "-" + name.AppName)
		if err != nil {
			notifyError(err)
			return
		}
		if len(pods.Items) == 0 {
			notifyError(catalystError.UnknownPod{
				Err: errors.Errorf("could not find any pods matching label '%s-%s' for environment '%s'",
					env.ReleaseName, name.AppName, env.ReleaseName),
			})
			return
		}

		for _, foundPod := range pods.Items {
			// assign the value to be used in go func
			containerName := name.ContainerName
			pod := foundPod
			numContainers++
			go func() {
				err = c.k8s.StreamContainerLogs(&pod, containerName, logChan)
				if err != nil {
					errChan <- err
				}
				logsTerminated++
				termChan <- logsTerminated
			}()
		}
	}

	for {
		select {
		case err := <-errChan:
			notifyError(err)
			return
		case msg, ok := <-logChan:
			notifyEvent(msg)
			if !ok {
				return
			}
		case terminated := <-termChan:
			if terminated == numContainers {
				return
			}
		}
	}
}

func (c *Catalyst) GetTokens() (*[]model.Token, error) {
	var tokens []model.Token
	err := c.repo.Find(&tokens).Error
	if err != nil {
		return nil, errors.New(err)
	}
	return &tokens, nil
}

func (c *Catalyst) CreateToken(token *model.Token) (string, error) {
	baseTokenValue, err := uuid.NewRandom()
	if err != nil {
		return "", errors.New(err)
	}

	// Persist first so the id is populated
	err = c.repo.SaveModel(token)
	if err != nil {
		return "", err
	}

	tokenValue := fmt.Sprintf("%d-%s", token.Id, baseTokenValue)
	encodedHash, err := c.hash.EncodedHash(tokenValue)
	if err != nil {
		return "", c.repo.DeleteDueToError(token, err)
	}
	token.EncodedHash = encodedHash

	err = c.repo.SaveModel(token)
	if err != nil {
		return "", c.repo.DeleteDueToError(token, err)
	}

	return tokenValue, nil
}

func (c *Catalyst) GetToken(id uint) (*model.Token, error) {
	token := &model.Token{}
	err := c.repo.Get(token, id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			ue := catalystError.UnknownEntry{Err: errors.Errorf("token %d does not exist", id)}
			return nil, ue
		}
		return nil, err
	}
	return token, nil
}
