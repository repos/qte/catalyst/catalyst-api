Helm charts for the catalyst control plane

## Deploy

### catalyst-api

You must either be SSHed into the control-plane or have kubectl access thereto.

```
helm install catalyst-api ./catalyst-api --set image.tag=[TAG]
```
