package dummy

import (
	_ "github.com/codeclysm/extract/v3"
	_ "github.com/gin-gonic/gin"
	_ "github.com/go-errors/errors"
	_ "github.com/go-playground/validator/v10"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/google/uuid"
	_ "github.com/onsi/ginkgo/v2"
	_ "github.com/onsi/gomega"
	_ "github.com/santhosh-tekuri/jsonschema/v5"
	_ "go.uber.org/mock/gomock"
	_ "golang.org/x/crypto/argon2"
	_ "gopkg.in/yaml.v3"
	_ "gorm.io/driver/mysql"
	_ "gorm.io/gorm"
	_ "helm.sh/helm/v3/pkg/action"
	_ "helm.sh/helm/v3/pkg/chart"
	_ "helm.sh/helm/v3/pkg/release"
	_ "k8s.io/api"
	_ "k8s.io/apimachinery"
	_ "k8s.io/client-go"
)

func main() {}
