package main

import (
	"fmt"
	"os/user"

	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/kubernetes"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/setup"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"
)

var Singletons *setup.Singletons

func main() {
	Singletons = &setup.Singletons{}
	usr, _ := user.Current()
	kubeConfig := utils.GetEnv("K8S_CONFIG_PATH", usr.HomeDir+"/.kube/config")
	var helmClientProvider setup.BackendDepProvider = func(s *setup.Singletons) any {
		return kubernetes.NewHelmClient(kubernetes.NewHelmWrapper(kubeConfig), s.KnownCharts)
	}
	var kubernetesClientProvider setup.BackendDepProvider = func(_ *setup.Singletons) any {
		return kubernetes.NewKubernetesClient(kubernetes.NewKubeWrapper(kubeConfig))
	}

	setup.WireUpCatalyst(
		Singletons,
		setup.BackendDependencies{
			Helm: helmClientProvider,
			K8s:  kubernetesClientProvider,
		},
	)

	port := utils.GetEnv("PORT", "8080")
	fmt.Println("catalyst-api server is running on port " + port)
	Singletons.Router.RunRouter("0.0.0.0:" + port)
}
