# API backend for the Catalyst project

https://gitlab.wikimedia.org/repos/test-platform/catalyst

## Local development 

### On Local Kubernetes Cluster 

#### Prerequisites

1. install [golang 1.23](https://go.dev/doc/install)
1. (optional: for debugging) install delve with `go install github.com/go-delve/delve/cmd/dlv@latest`
1. install [minikube](https://minikube.sigs.k8s.io/docs/start/?arch=%2Flinux%2Fx86-64%2Fstable%2Fbinary+download)
1. install [kubectl](https://kubernetes.io/docs/tasks/tools/)
1. install [helm](https://helm.sh/docs/intro/install/)
1. (optional: for cluster visibility) install [k9s](https://k9scli.io/topics/install/)

#### Running

Run:

```
./dev
```

from the repo directory and wait for the deployment to become stable.

`dev` does the following:

- ensures minikube is started with the ingress addon
- builds a development version of the catalyst-api container images for local use with minikube
- configures and deploys the catalyst-api helm chart on minikube
  - adds minikubes kubeconfig to the chart values
  - sets up ingress from your local machine into the cluster
    - open up `_skaffold_env.yaml` `ingress.hostname` to see the URL you can point curl, postman, or your browser to
- forwards the delve debug port to port 40000 on the local machine

#### Making requests

You can use `./dev-curl` to make authenticated web requests to the api running on minikube. `dev-curl` will show you the curl command it's about to run, before it runs it. E.g.

```
./dev-curl /api/apiTokens -X POST -d '{"description": "test"}'
```

#### Debugging

When starting `dev` with the `debug` parameter (i.e. `./dev debug`), skaffold exposes the port used by [delve], 40000, which is forwarded to your development machine. You can attach to the debugger to set breakpoints and step through code in VSCode by clicking: Run > Start Debugging, or by pressing F5.

Please note, when running in debug mode, skaffold does not automatically recompile and redeploy on source code changes. This is so your IDE does not get unexpectedly get disconnected from the remote debugger.

## Catalyst design diagram

The following diagram shows the three layers Catalyst is structured around:

![alt arch](docs/Catalyst.png "Catalyst")

## BDD tests

The `bdd` directory contains suites of functional BDD tests. These tests run against Catalyst's
REST API and verify the different use cases/workflows for a client of the API, treating Catalyst as
a black box.

Backend dependencies (e.g. Kubernetes) are mocked, with the exception of DB persistence which needs
to be provided externally to the test code. Generated mocks can be recreated with `make mocks`.

The testing framework used is [Ginkgo](https://github.com/onsi/ginkgo), supplemented by matcher library
[Gomega](https://github.com/onsi/gomega) and mocking library [gomock](https://github.com/uber-go/mock).

### Running the tests

The project's `Makefile` provides a target to run all the BDD tests. It will provision a local MariaDB
instance and inject the test code with the necessary configuration to connect to it. You can run
this target using command: `make bdd`.

It is also possible to start the MariDB instance directly with: `make db`. This can be useful to
quickly get a DB for local development for example.

If you want to clean up the DB, you can do that with: `make db-clean` or simply: `make clean`.