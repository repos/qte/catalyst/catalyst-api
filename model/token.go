package model

import "time"

type TokenCreationResponse struct {
	Token
	Value string `json:"token"`
}

type Token struct {
	Id          uint      `json:"id" gorm:"primaryKey"`
	Description string    `json:"description" binding:"required"`
	CreatedAt   time.Time `json:"createdAt"` // UTC
	EncodedHash string    `json:"-" gorm:"type:varchar(120)"`
}

func (tk *Token) Persistable() {}
