package model

import (
	"encoding/json"
	"time"

	"github.com/go-errors/errors"
	"gitlab.wikimedia.org/repos/test-platform/catalyst/catalyst-api/utils"
	"gorm.io/gorm"
	"helm.sh/helm/v3/pkg/chart"
)

const (
	EnvStarting = "starting"
	EnvRunning  = "running"
	EnvFailed   = "failed"
	EnvDeleted  = "deleted"
)

type Environment struct {
	BaseEnvironment
	RequestOverrides map[string]any `json:"-" gorm:"-"`
	Chart            *chart.Chart   `json:"-" gorm:"-"`
}

type BaseEnvironment struct {
	Id          uint            `json:"id" gorm:"primaryKey"`
	OwnerId     *uint           `json:"ownerID" gorm:"foreignKey"`
	Name        string          `json:"name" binding:"required" gorm:"type:varchar(80);uniqueIndex:name_deletedAt"`
	ChartName   string          `json:"chartName" binding:"required,knownchart" gorm:"type:varchar(80)"`
	Status      string          `json:"status" gorm:"type:varchar(20)"`
	ReleaseName string          `json:"releaseName" gorm:"type:varchar(80)"`
	CreatedAt   time.Time       `json:"createdAt"` // UTC
	DeletedAt   gorm.DeletedAt  `json:"deletedAt" gorm:"uniqueIndex:name_deletedAt;default:'0000-00-00';not null"`
	RawValues   json.RawMessage `json:"values" binding:"required" gorm:"column:values"`
}

func (env *Environment) Persistable() {}

func (env *Environment) IsDeleted() bool {
	return env.Status == EnvDeleted
}

func (env *Environment) AddChart(chart *chart.Chart) {
	// Apply overrides from environment to the chart values
	if len(env.RequestOverrides) > 0 {
		utils.DeepMerge(chart.Values, env.RequestOverrides)
	}
	env.Chart = chart
}

func (env *Environment) Values() map[string]any {
	if env.Chart == nil {
		return make(map[string]any)
	}
	return env.Chart.Values
}

func (env *Environment) UnmarshalJSON(data []byte) error {
	baseEnv := BaseEnvironment{}
	if err := json.Unmarshal(data, &baseEnv); err != nil {
		return errors.New(err)
	}
	newEnv := Environment{
		BaseEnvironment: baseEnv,
	}
	err := json.Unmarshal(baseEnv.RawValues, &newEnv.RequestOverrides)
	if err != nil {
		return errors.New(err)
	}

	*env = newEnv
	return nil
}
