package utils

import (
	"os"
	"strconv"

	"github.com/go-errors/errors"
)

func GetEnv(key, def string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return def
}

func FileExists(file string) bool {
	_, err := os.Stat(file)
	return !errors.Is(err, os.ErrNotExist)
}

func HandleError(err error) {
	if err != nil {
		panic(err)
	}
}

func ParseUint(val any) (uint, error) {
	valStr := val.(string)
	val64, err := strconv.ParseUint(valStr, 10, 64)
	if err != nil {
		return 0, errors.New(err)
	}
	return uint(val64), nil
}

// DeepMerge src into dst (i.e. src values take precedence)
func DeepMerge(dst map[string]any, src map[string]any) {
	for key, val := range src {
		dstN, notDstLeaf := dst[key].(map[string]any)
		srcN, notSrcLeaf := val.(map[string]any)
		if notDstLeaf && notSrcLeaf {
			DeepMerge(dstN, srcN)
		} else {
			dst[key] = val
		}
	}
}

// Get Extracts value from `val`, which needs to be indexable by strings (i.e. a map[string])
func Get(val any, indices ...string) (any, error) {
	res := val
	for _, index := range indices {
		switch typedVal := res.(type) {
		case map[string]any:
			valAtIndex, exists := typedVal[index]
			if !exists {
				err := errors.Errorf(`value "%v" does not contain key "%v"`, typedVal, index)
				return nil, err
			}
			res = valAtIndex
		default:
			err := errors.Errorf(`value "%v" of type "%T" is not indexable`, typedVal, typedVal)
			return nil, err
		}
	}
	return res, nil
}
